using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Adam.SecretHitler.Cfr
{
    public class CfrTrainedStrategy : IStrategy
    {
        private static readonly ConcurrentDictionary<string, Dictionary<CfrInformationSet, double[]>[]> cache
            = new ConcurrentDictionary<string, Dictionary<CfrInformationSet, double[]>[]>();

        private readonly Dictionary<CfrInformationSet, double[]>[] aggregateStrategies;
        private readonly Random random;
        private Role role;
        private CfrInformationSet baseInfoset;

        public CfrTrainedStrategy(Random random, string inputFile)
        {
            if (!cache.ContainsKey(inputFile))
            {
                var minimizer = CfrMinimizer.Load(inputFile);
                cache.TryAdd(inputFile, minimizer.AggregateStrategies);
            }

            aggregateStrategies = cache[inputFile];
            this.random = random;
        }

        public void SetUp(
            PublicState state,
            Player self,
            Role role,
            Player hitler,
            ImmutableArray<Player> fascists)
        {
            if (role == Role.None)
            {
                throw new ArgumentOutOfRangeException(nameof(role));
            }
            this.role = role;
            baseInfoset = CfrInformationSet.FromPublicState(state);
        }

        public Player CallSpecialElection(ImmutableArray<Player> eligible)
        {
            var action = GetAction(RoundStage.ExecutiveAction);
            if (eligible.Contains(action.Player))
            {
                return action.Player;
            }

            return eligible[random.Next(0, eligible.Length)];
        }

        public Party ConcludeInvestigation(Player investigated, Party partyMembership)
        {
            return GetAction(RoundStage.ConcludeInvestigation).Party;
        }

        public Party DiscardAsChancellor(
            ImmutableArray<Party> hand,
            out ImmutableArray<Party> chancellorComment,
            out bool wantsVeto)
        {
            var action = GetAction(RoundStage.DiscardAsChancellor);
            wantsVeto = action.Vote;
            if (hand.Contains(action.Party))
            {
                return action.Party;
            }

            return hand[^1];
        }

        public Party DiscardAsPresident(
            ImmutableArray<Party> hand,
            out ImmutableArray<Party> presidentComment,
            out bool wouldVeto)
        {
            var action = GetAction(RoundStage.DiscardAsPresident);
            wouldVeto = action.Vote;
            if (hand.Contains(action.Party))
            {
                return action.Party;
            }

            return hand[^1];
        }

        public Player ExecutePlayer(ImmutableArray<Player> executable)
        {
            var action = GetAction(RoundStage.ExecutiveAction);
            if (executable.Contains(action.Player))
            {
                return action.Player;
            }

            return executable[random.Next(0, executable.Length)];
        }

        public Player InvestigateLoyalty(ImmutableArray<Player> investigable)
        {
            var action = GetAction(RoundStage.DiscardAsChancellor);
            if (investigable.Contains(action.Player))
            {
                return action.Player;
            }

            return investigable[random.Next(0, investigable.Length)];
        }

        public void OnRound(PublicState state)
        {
            baseInfoset = CfrInformationSet.FromPublicState(state);
        }

        public Player NominateChancellor(ImmutableArray<Player> eligible)
        {
            var action = GetAction(RoundStage.NominateChancellor);
            if (eligible.Contains(action.Player))
            {
                return action.Player;
            }

            return eligible[random.Next(0, eligible.Length)];
        }

        public bool Vote(Player presidentElect, Player chancellorElect)
        {
            return GetAction(RoundStage.Vote).Vote;
        }

        private CfrAction GetAction(RoundStage stage)
        {
            var infoset = baseInfoset;
            infoset.Stage = stage;
            var actions = CfrGame.AllActions(infoset);
            if (!actions.Any())
            {
                throw new InvalidOperationException("Somehow, there's no action that can be taken.");
            }

            var strategy = aggregateStrategies[CfrGame.RoleIds[role]];
            if (!strategy.TryGetValue(infoset, out var weights))
            {
                // Console.WriteLine($"State [{infoset}] not found in self-play history. Using random action.");
                return actions[random.Next(actions.Length)];
            }

            double totalWeight = weights.Sum();
            if (totalWeight == 0)
            {
                return actions[random.Next(weights.Length)];
            }

            double pick = random.NextDouble() * totalWeight;
            for (int i = 0; i < weights.Length; i++)
            {
                if (pick <= weights[i])
                {
                    return actions[i];
                }
            }

            return actions.First();
        }
    }
}
