using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace Adam.SecretHitler.Cfr
{
    public class CfrMinimizer
    {
        // The number of players in the game
        private readonly int strategyCount;

        // The accumulated regrets for each action. The data is stored in the format
        //     player -> information set id -> list of actions
        public Dictionary<CfrInformationSet, double[]>[] AggregateRegrets { get; }

        // The number of times each action was selected during the course of training
        //     The data is stored in the format player -> information set id ->
        //     list of actions
        public Dictionary<CfrInformationSet, double[]>[] AggregateStrategies { get; }
        private readonly Random random = new Random();
        private long totalIterations;

        /**
         * Initializes the CFR algorithm
         */
        public CfrMinimizer()
        {
            strategyCount = CfrGame.RoleIds.Count; // Hitler, Fascist, Liberal
            AggregateRegrets = new Dictionary<CfrInformationSet, double[]>[strategyCount];
            AggregateStrategies = new Dictionary<CfrInformationSet, double[]>[strategyCount];
            for (int i = 0; i < strategyCount; ++i)
            {
                AggregateRegrets[i] = new Dictionary<CfrInformationSet, double[]>();
                AggregateStrategies[i] = new Dictionary<CfrInformationSet, double[]>();
            }
            totalIterations = 0;
        }

        public CfrMinimizer(
            Dictionary<CfrInformationSet, double[]>[] regrets,
            Dictionary<CfrInformationSet, double[]>[] strategies,
            int totalIterations)
        {
            strategyCount = regrets.Length;
            AggregateRegrets = regrets;
            AggregateStrategies = strategies;
            this.totalIterations = totalIterations;
        }

        public static CfrMinimizer Load(string filename)
        {
            using var regretsFile = File.OpenRead($"{filename}.regrets");
            var regrets = Deserialize(regretsFile);
            using var strategiesFile = File.OpenRead($"{filename}.strategies");
            var strategies = Deserialize(strategiesFile);
            var totalIterations = int.Parse(filename.Split('-').Last());
            Console.WriteLine($"Loaded {filename} on iteration {totalIterations}.");
            return new CfrMinimizer(regrets, strategies, totalIterations);
        }

        /**
         * Runs CFR for an infinite number of iterations. The computed strategy will
         * periodically be saved to an output file.
         *
         * @param outputFile the file to save the current strategy to
         * @param itersPerSave the number of iterations before the strategy
         *     should be checkpointed
         * @param itersPerUpdate the number of iterations before an update message
         *     is printed to the console
         */
        public void Solve(
            string outputFolder,
            long itersPerSave,
            long itersPerUpdate,
            bool randomSample = true)
        {
            // initialize training counters
            long saveCounter = 0;

            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }

            Console.WriteLine("Training...");

            while (true)
            {
                // train the model
                Train(itersPerUpdate, randomSample);

                // print an update to the console
                saveCounter += itersPerUpdate;
                totalIterations += itersPerUpdate;
                Console.WriteLine($"Completed iteration: {totalIterations}");

                // save a checkpoint
                if (saveCounter >= itersPerSave)
                {
                    saveCounter %= itersPerSave;

                    string outputName = Path.Join(outputFolder, $"cfr-{totalIterations}");
                    Console.WriteLine($"Saving to {outputName}...");
                    Save(outputName);
                }
            }
        }

        /**
         * Runs CFR for the given number of iterations
         *
         * @param iterations the number of iterations to run
         */
        public void Train(long iterations, bool randomSample = true)
        {
            // train on the game
            for (int i = 0; i < iterations; ++i)
            {
                var game = CfrGame.BeginGame();
                double[] probabilities = Enumerable.Repeat(1.0, strategyCount).ToArray();
                if (randomSample)
                {
                    Train(game, probabilities);
                }
                else
                {
                    TrainFull(game, probabilities);
                }
            }
        }

        public static void Serialize(Dictionary<CfrInformationSet, double[]>[] dictionary, Stream stream)
        {
            new BinaryFormatter().Serialize(stream, dictionary);
        }

        public static Dictionary<CfrInformationSet, double[]>[] Deserialize(Stream stream)
        {
            return (Dictionary<CfrInformationSet, double[]>[])new BinaryFormatter().Deserialize(stream);
        }

        /**
         * Saves the current strategy to the given file
         *
         * @param filename the file to save to
         */
        public void Save(string filename)
        {
            Utils.EnsureDirectoryExists(filename);

            using (var fs = File.Create($"{filename}.regrets"))
            {
                Serialize(AggregateRegrets, fs);
            }
            using (var fs = File.Create($"{filename}.strategies"))
            {
                Serialize(AggregateStrategies, fs);
            }
        }

        /**
        * Calculates the players current strategy for a given information set
        *
        * @param player the player to move
        * @param id the information set's id
        * @param actions the set of possible actions
        *
        * @return the probability distribution over the player's actions
        */
        private double[] GetStrategy(int player, CfrInformationSet id, IList<CfrAction> actions)
        {
            // load historical data about this information set
            if (player < 0 || player > strategyCount)
            {
                Console.WriteLine(player);
                Console.WriteLine(id);
                return new double[0];
            }
            var cumulativeRegrets = AggregateRegrets[player][id];
            double[] strategy = new double[actions.Count];
            double normalizingSum = 0;

            // choose actions with probability in proportion to their regret
            for (int action = 0; action < actions.Count; ++action)
            {
                double regret;
                if (cumulativeRegrets[action] > 0)
                {
                    regret = cumulativeRegrets[action];
                }
                else
                {
                    regret = 0;
                }
                strategy[action] = regret;
                normalizingSum += regret;
            }

            // normalize the strategy into a probability distribution
            for (int action = 0; action < actions.Count; ++action)
            {
                if (normalizingSum > 0)
                {
                    strategy[action] /= normalizingSum;
                }
                else
                {
                    strategy[action] = 1.0 / actions.Count;
                }
            }

            return strategy;
        }

        /**
         * Analyzes one node of the game tree and recursively updates the strategy
         *
         * @param payouts the abstract strategy game to train
         * @param probabilities the i-th element is the probability that player i
         *     causes this information set to occur
         */
        private double[] Train(CfrGame game, double[] probabilities)
        {
            // check if the game has ended
            if (game.IsTerminalState())
            {
                return game.GetPayout();
            }

            int player = game.StrategyToAct();
            // create the information set node
            var id = game.GetInformationSet();
            var allActions = CfrGame.AllActions(game.GetInformationSet());
            if (!AggregateStrategies[player].ContainsKey(id))
            {
                AggregateStrategies[player].Add(id, new double[allActions.Length]);
            }

            if (!AggregateRegrets[player].ContainsKey(id))
            {
                AggregateRegrets[player].Add(id, new double[allActions.Length]);
            }

            // determine the player's strategy
            var strategy = GetStrategy(player, id, allActions);
            double[][] actionUtilities = new double[allActions.Length][];
            double[] nodeUtilities = new double[strategyCount]; // zero initialized

            var actions = game.Actions();
            int action;
            do
            {
                action = random.Next(allActions.Length);
            }
            while (!actions.Contains(allActions[action]));

            //for (int action = 0; action < actions.Length; ++action)
            {
                // branch on the player's action
                var gameCopy = game;
                // Console.WriteLine($"player {player}, action {actions[action]}");
                gameCopy.MakeMove(allActions[action]);
                double[] probabilitiesCopy = new double[probabilities.Length];
                probabilities.CopyTo(probabilitiesCopy, 0);

                probabilitiesCopy[player] *= strategy[action];

                // update utilities
                actionUtilities[action] = Train(gameCopy, probabilitiesCopy);
                for (int agent = 0; agent < strategyCount; ++agent)
                {
                    nodeUtilities[agent] += strategy[action] * actionUtilities[action][agent];
                }
            }

            // accumulate counterfactual regret
            //for (var action = 0; action < actions.Length; ++action)
            {
                // calculate the counterfactual probability
                double counterfactual = 1.0;
                for (int agent = 0; agent < strategyCount; ++agent)
                {
                    if (agent != player)
                    {
                        counterfactual *= probabilities[agent];
                    }
                }

                // update regrets
                var regret = actionUtilities[action][player] - nodeUtilities[player];
                AggregateRegrets[player][id][action] += counterfactual * regret;
                AggregateStrategies[player][id][action] += counterfactual * strategy[action];
            }

            return nodeUtilities;
        }

        private double[] TrainFull(CfrGame game, double[] probabilities)
        {
            // check if the game has ended
            if (game.IsTerminalState())
            {
                return game.GetPayout();
            }

            int player = game.StrategyToAct();
            // create the information set node
            var id = game.GetInformationSet();
            var allActions = CfrGame.AllActions(game.GetInformationSet());
            if (!AggregateStrategies[player].ContainsKey(id))
            {
                AggregateStrategies[player].Add(id, new double[allActions.Length]);
            }

            if (!AggregateRegrets[player].ContainsKey(id))
            {
                AggregateRegrets[player].Add(id, new double[allActions.Length]);
            }

            // determine the player's strategy
            var strategy = GetStrategy(player, id, allActions);
            double[][] actionUtilities = new double[allActions.Length][];
            double[] nodeUtilities = new double[strategyCount]; // zero initialized

            var actions = game.Actions();
            // Recursively train on each action
            for (int action = 0; action < allActions.Length; ++action)
            {
                if (!actions.Contains(allActions[action]))
                {
                    continue;
                }
                // branch on the player's action
                var gameCopy = game;
                // Console.WriteLine($"player {player}, action {actions[action]}");
                gameCopy.MakeMove(actions[action]);
                double[] probabilitiesCopy = new double[probabilities.Length];
                probabilities.CopyTo(probabilitiesCopy, 0);

                probabilitiesCopy[player] *= strategy[action];

                // update utilities
                actionUtilities[action] = TrainFull(gameCopy, probabilitiesCopy);
                for (int agent = 0; agent < strategyCount; ++agent)
                {
                    nodeUtilities[agent] += strategy[action] * actionUtilities[action][agent];
                }
            }

            // accumulate counterfactual regret
            for (var action = 0; action < actions.Length; ++action)
            {
                // calculate the counterfactual probability
                double counterfactual = 1.0;
                for (int agent = 0; agent < strategyCount; ++agent)
                {
                    if (agent != player)
                    {
                        counterfactual *= probabilities[agent];
                    }
                }

                // update regrets
                var regret = actionUtilities[action][player] - nodeUtilities[player];
                AggregateRegrets[player][id][action] += counterfactual * regret;
                AggregateStrategies[player][id][action] += counterfactual * strategy[action];
            }

            return nodeUtilities;
        }
    }
}
