using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;

namespace Adam.SecretHitler.Cfr
{
    public struct CfrGame
    {
        public static readonly ImmutableDictionary<Role, int> RoleIds = new Dictionary<Role, int>
        {
            { Role.Hitler, 0 },
            { Role.Fascist, 1 },
            { Role.Liberal, 2 }
        }.ToImmutableDictionary();

        private GameState state;
        private RoundState round;
        private ImmutableArray<int> playerRoles;


        public static CfrGame BeginGame()
        {
            var random = ImmutableRandom.Create();
            var strategies = ImmutableArray.Create<(Role, IStrategy)>(
                (Role.Hitler, new CfrStrategy(0)),
                (Role.Fascist, new CfrStrategy(1)),
                (Role.Fascist, new CfrStrategy(2)),
                (Role.Liberal, new CfrStrategy(3)),
                (Role.Liberal, new CfrStrategy(4)),
                (Role.Liberal, new CfrStrategy(5)),
                (Role.Liberal, new CfrStrategy(6)),
                (Role.Liberal, new CfrStrategy(7))
            );
            var state = Game.SetUp(random, strategies);
            var round = RoundState.FromGameState(state);
            var playerRoles = state.Players.Select(x => ((CfrStrategy)x.strategy).OriginalIndex)
                .ToImmutableArray();
            // pass the initial presidential candidacy
            Game.NextMove(state, ref round);
            return new CfrGame
            {
                state = state,
                round = round,
                playerRoles = playerRoles
            };
        }

        public void MakeMove(CfrAction move)
        {
            Debug.Assert(!IsTerminalState());

            if (round.Stage == RoundStage.PassCandidacy)
            {
                Game.NextMove(state, ref round); // choose president, no player action
            }

            var playerToAct = PlayerToAct();
            var nextPlayerStrategy = (CfrStrategy)state.Players[playerRoles.IndexOf(playerToAct)].strategy;
            nextPlayerStrategy.SetNextAction(move);
            Game.NextMove(state, ref round);

            if (round.Stage == RoundStage.End)
            {
                state = Game.ApplyRound(state, round);
                round = RoundState.FromGameState(state);
            }

            if (round.Stage == RoundStage.PassCandidacy)
            {
                Game.NextMove(state, ref round); // choose president, no player action
            }

            // TODO: Remove this debugger break
            if (round.Stage == RoundStage.PassCandidacy)
            {
                Debugger.Break();
            }
        }

        public double[] GetPayout()
        {
            Debug.Assert(IsTerminalState());

            switch (Game.GetWinner(state))
            {
                case EndCondition.LiberalPolicies:
                case EndCondition.HitlerExecuted:
                    return new double[3] { -1, -1, 1 };
                case EndCondition.FascistPolicies:
                case EndCondition.HitlerElected:
                    return new double[3] { 1, 1, -1 };
            }

            throw new NotImplementedException();
        }

        public CfrInformationSet GetInformationSet()
        {
            var infoset = CfrInformationSet.FromPublicState(state.Public);
            infoset.Stage = round.Stage;
            return infoset;
        }

        public bool IsTerminalState()
        {
            return Game.GetWinner(state) != EndCondition.None;
        }

        public int NumPlayers()
        {
            return 3;
        }

        public int StrategyToAct()
        {
            switch (PlayerToAct())
            {
                case 0:
                    return 0; // hitler
                case 1:
                case 2:
                    return 1; // fascist
                default:
                    return 2; // liberal
            }
        }

        public int PlayerToAct()
        {
            switch (round.Stage)
            {
                case RoundStage.NominateChancellor:
                    return playerRoles[round.PresidentElect.Index];
                case RoundStage.Vote:
                    return playerRoles[round.VotingPlayer.Index];
                case RoundStage.DiscardAsPresident:
                case RoundStage.ExecutiveAction:
                case RoundStage.ConcludeInvestigation:
                    return playerRoles[round.PresidentElect.Index];
                case RoundStage.DiscardAsChancellor:
                    return playerRoles[round.ChancellorElect.Index];
                default:
                    Console.WriteLine("player to act is zero");
                    Console.WriteLine(round.Stage);
                    return 0;
            }
        }

        public static ImmutableArray<CfrAction> AlivePlayerActions(CfrInformationSet infoset)
        {
            var players = Enumerable.Range(0, infoset.PlayerCount);
            var dead = players.Select(p => infoset.Dead[p]);

            return players.Where(p => !infoset.Dead[p])
                .Select(p => new CfrAction { Player = new Player { Index = p } })
                .ToImmutableArray();
        }

        public static ImmutableArray<CfrAction> AllActions(CfrInformationSet infoset)
        {
            switch (infoset.Stage)
            {
                case RoundStage.PassCandidacy:
                case RoundStage.End:
                    return ImmutableArray.Create<CfrAction>();
                case RoundStage.NominateChancellor:
                    return AlivePlayerActions(infoset);
                case RoundStage.Vote:
                    return CfrAction.YesNo();
                case RoundStage.DiscardAsPresident:
                case RoundStage.DiscardAsChancellor:
                    return CfrAction.Discard();
                case RoundStage.ExecutiveAction:
                    return AlivePlayerActions(infoset);
                case RoundStage.ConcludeInvestigation:
                    return CfrAction.Investigation();
            }

            throw new NotImplementedException();
        }

        public ImmutableArray<CfrAction> Actions()
        {
            switch (round.Stage)
            {
                case RoundStage.PassCandidacy:
                    return ImmutableArray.Create<CfrAction>();
                case RoundStage.NominateChancellor:
                    var ce = Game.GetChancellorEligible(ref state, round.PresidentElect);
                    return ce.Select(p => new CfrAction { Player = p }).ToImmutableArray();
                case RoundStage.Vote:
                    return CfrAction.YesNo();
                case RoundStage.DiscardAsPresident:
                case RoundStage.DiscardAsChancellor:
                    return CfrAction.Discard();
                case RoundStage.ExecutiveAction:
                    return PowerActions();
                case RoundStage.ConcludeInvestigation:
                    return CfrAction.Investigation();
            }

            throw new NotImplementedException();
        }

        private ImmutableArray<CfrAction> PowerActions()
        {
            switch (round.Power)
            {
                case PresidentialPower.CallSpecialElection:
                case PresidentialPower.InvestigateLoyalty:
                    var ie = Game.GetInvestigableSpecialElectable(ref state, round.PresidentElect);
                    return ie.Select(p => new CfrAction { Player = p }).ToImmutableArray();
                case PresidentialPower.Execution:
                    var e = Game.GetExecutablePlayers(state);
                    return e.Select(p => new CfrAction { Player = p }).ToImmutableArray();
            }

            throw new NotImplementedException();
        }
    }
}
