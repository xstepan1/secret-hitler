using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace Adam.SecretHitler.Cfr
{
    [Serializable]
    public struct CfrInformationSet : IEquatable<CfrInformationSet>
    {
        public int PlayerCount;
        public bool[] Dead;
        public int FascistPolicyCount;
        public int LiberalPolicyCount;
        public int ElectionTracker;
        public RoundStage Stage;

        public static CfrInformationSet FromPublicState(PublicState state)
        {
            return new CfrInformationSet
            {
                Dead = state.Dead.ToArray(),
                ElectionTracker = state.ElectionTracker,
                FascistPolicyCount = state.FascistPolicyCount,
                LiberalPolicyCount = state.LiberalPolicyCount,
                PlayerCount = state.PlayerCount,
                Stage = RoundStage.Invalid
            };
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(PlayerCount);
            sb.Append(' ');
            sb.Append(FascistPolicyCount);
            sb.Append('|');
            sb.Append(LiberalPolicyCount);
            sb.Append(' ');
            sb.Append(ElectionTracker);
            sb.Append(' ');
            foreach (var isDead in Dead) {
                sb.Append(isDead ? 'D' : 'A');
            }
            sb.Append(' ');
            sb.Append(Stage);
            return sb.ToString();
        }

        public bool Equals([AllowNull] CfrInformationSet other)
        {
            return PlayerCount.Equals(other.PlayerCount)
                && Enumerable.SequenceEqual(Dead, other.Dead)
                && FascistPolicyCount.Equals(other.FascistPolicyCount)
                && LiberalPolicyCount.Equals(other.LiberalPolicyCount)
                && ElectionTracker.Equals(other.ElectionTracker)
                && Stage.Equals(other.Stage);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(PlayerCount, FascistPolicyCount, LiberalPolicyCount, ElectionTracker, Stage);
        }

        public override bool Equals(object? obj)
        {
            if (obj is CfrInformationSet cis)
            {
                return Equals(cis);
            }

            return false;
        }
    }
}
