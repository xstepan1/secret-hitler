using System.Collections.Immutable;
using System.Text;

namespace Adam.SecretHitler.Cfr
{
    public struct CfrAction
    {
        public Party Party;
        public bool Vote;
        public Player Player;

        public override string ToString()
        {
            return $"{Party} {Vote} {Player}";
        }

        public static readonly CfrAction Yes = new CfrAction()
        {
            Party = Party.None,
            Vote = true,
            Player = Player.None
        };

        public static readonly CfrAction No = new CfrAction()
        {
            Party = Party.None,
            Vote = false,
            Player = Player.None
        };

        public static ImmutableArray<CfrAction> YesNo()
        {
            return ImmutableArray.Create(Yes, No);
        }

        public static ImmutableArray<CfrAction> Discard()
        {
            return ImmutableArray.Create(
                new CfrAction()
                {
                    Party = Party.Fascist,
                    Vote = false,
                    Player = Player.None
                },
                new CfrAction()
                {
                    Party = Party.Liberal,
                    Vote = false,
                    Player = Player.None
                },
                new CfrAction()
                {
                    Party = Party.Fascist,
                    Vote = true,
                    Player = Player.None
                },
                new CfrAction()
                {
                    Party = Party.Liberal,
                    Vote = true,
                    Player = Player.None
                }
            );
        }

        public static ImmutableArray<CfrAction> Investigation()
        {

            return ImmutableArray.Create(
                new CfrAction()
                {
                    Party = Party.None,
                    Vote = false,
                    Player = Player.None
                },
                new CfrAction()
                {
                    Party = Party.Fascist,
                    Vote = false,
                    Player = Player.None
                },
                new CfrAction()
                {
                    Party = Party.Liberal,
                    Vote = false,
                    Player = Player.None
                }
            );
        }
    }
}
