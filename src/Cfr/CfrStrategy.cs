using System;
using System.Collections.Immutable;

namespace Adam.SecretHitler.Cfr
{
    public class CfrStrategy : IStrategy
    {
        private CfrAction nextAction;
        public int OriginalIndex;

        public CfrStrategy(int index) {
            this.OriginalIndex = index;
        }

        public void SetUp(
            PublicState state,
            Player self,
            Role role,
            Player hitler,
            ImmutableArray<Player> fascists)
        {
        }

        public void SetNextAction(CfrAction action)
        {
            nextAction = action;
        }

        public Player CallSpecialElection(ImmutableArray<Player> eligible)
        {
            if (eligible.Contains(nextAction.Player))
            {
                return nextAction.Player;
            }

            throw new InvalidOperationException();
        }

        public Party ConcludeInvestigation(Player investigated, Party partyMembership)
        {
            return nextAction.Party;
        }

        public Party DiscardAsChancellor(ImmutableArray<Party> hand, out ImmutableArray<Party> chancellorComment, out bool wantsVeto)
        {
            wantsVeto = nextAction.Vote;
            if (hand.Contains(nextAction.Party))
            {
                return nextAction.Party;
            }

            return hand[^1];
        }

        public Party DiscardAsPresident(ImmutableArray<Party> hand, out ImmutableArray<Party> presidentComment, out bool wouldVeto)
        {
            wouldVeto = nextAction.Vote;
            if (hand.Contains(nextAction.Party))
            {
                return nextAction.Party;
            }

            return hand[^1];
        }

        public Player ExecutePlayer(ImmutableArray<Player> executable)
        {
            if (executable.Contains(nextAction.Player))
            {
                return nextAction.Player;
            }

            throw new InvalidOperationException();
        }

        public Player InvestigateLoyalty(ImmutableArray<Player> investigable)
        {
            if (investigable.Contains(nextAction.Player))
            {
                return nextAction.Player;
            }

            throw new InvalidOperationException();
        }

        public void OnRound(PublicState state)
        {
        }

        public Player NominateChancellor(ImmutableArray<Player> eligible)
        {
            if (eligible.Contains(nextAction.Player))
            {
                return nextAction.Player;
            }

            throw new InvalidOperationException();
        }

        public bool Vote(Player presidentElect, Player chancellorElect)
        {
            return nextAction.Vote;
        }
    }
}
