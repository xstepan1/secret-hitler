namespace Adam.SecretHitler
{
    public enum RoundStage
    {
        Invalid,
        PassCandidacy,
        NominateChancellor,
        Vote,
        DiscardAsPresident,
        DiscardAsChancellor,
        ExecutiveAction,
        ConcludeInvestigation,
        End
    }
}
