using System.Collections.Immutable;

namespace Adam.SecretHitler
{
    public abstract class StrategyWrapper : IStrategy
    {
        protected IStrategy inner = new NotImplementedStrategy();

        public Player CallSpecialElection(ImmutableArray<Player> eligible)
        {
            return inner.CallSpecialElection(eligible);
        }

        public Party ConcludeInvestigation(Player investigated, Party partyMembership)
        {
            return inner.ConcludeInvestigation(investigated, partyMembership);
        }

        public Party DiscardAsChancellor(ImmutableArray<Party> hand, out ImmutableArray<Party> chancellorComment, out bool wantsVeto)
        {
            return inner.DiscardAsChancellor(hand, out chancellorComment, out wantsVeto);
        }

        public Party DiscardAsPresident(ImmutableArray<Party> hand, out ImmutableArray<Party> presidentComment, out bool wouldVeto)
        {
            return inner.DiscardAsPresident(hand, out presidentComment, out wouldVeto);
        }

        public Player ExecutePlayer(ImmutableArray<Player> executable)
        {
            return inner.ExecutePlayer(executable);
        }

        public Player InvestigateLoyalty(ImmutableArray<Player> investigable)
        {
            return inner.InvestigateLoyalty(investigable);
        }

        public void OnRound(PublicState state)
        {
            inner.OnRound(state);
        }

        public Player NominateChancellor(ImmutableArray<Player> eligible)
        {
            return inner.NominateChancellor(eligible);
        }

        public virtual void SetUp(
            PublicState state,
            Player self,
            Role role,
            Player hitler,
            ImmutableArray<Player> fascists)
        {
            inner.SetUp(state, self, role, hitler, fascists);
        }

        public bool Vote(Player presidentElect, Player chancellorElect)
        {
            return inner.Vote(presidentElect, chancellorElect);
        }
    }
}
