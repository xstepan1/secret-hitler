using System.Collections.Immutable;

namespace Adam.SecretHitler
{
    public struct RoundSummary
    {
        public static readonly RoundSummary Initial = new RoundSummary
        {
            VotedJa = ImmutableArray.Create<Player>(),
            VotedNein = ImmutableArray.Create<Player>(),
            DidPresidentVeto = false,
            DidChancellorVeto = false,
            IsElectionSuccessful = false,
            IsVoteOfPeople = false,
            IsSpecialElection = false,
            IsVetoed = false,
            PresidentElect = Player.None,
            ChancellorElect = Player.None,
            ElectedPolicy = Party.None,
            PresidentComment = ImmutableArray.Create<Party>(),
            ChancellorComment = ImmutableArray.Create<Party>(),
            Power = PresidentialPower.None,
            InvestigateLoyaltyComment = Party.None,
            SpecialElectionChoice = Player.None,
            ExecutedPlayer = Player.None,
            OriginalOrder = Player.None
        };

        public static RoundSummary FromRoundState(RoundState round)
        {
            var summary = Initial;
            summary.VotedJa = round.VotedJa;
            summary.VotedNein = round.VotedNein;
            summary.DidPresidentVeto = round.DoesPresidentVeto;
            summary.DidChancellorVeto = round.DoesChancellorVeto;
            summary.IsElectionSuccessful = round.IsElectionSuccessful;
            summary.IsVoteOfPeople = round.IsVoteOfPeople;
            summary.IsSpecialElection = round.IsSpecialElection;
            summary.IsVetoed = round.IsVetoed;
            summary.PresidentElect = round.PresidentElect;
            summary.ChancellorElect = round.ChancellorElect;
            summary.ElectedPolicy = round.ElectedPolicy;
            summary.PresidentComment = round.PresidentComment;
            summary.ChancellorComment = round.ChancellorComment;
            summary.Power = round.Power;
            summary.InvestigateLoyaltyComment = round.InvestigateLoyaltyComment;
            summary.SpecialElectionChoice = round.SpecialElectionChoice;
            summary.ExecutedPlayer = round.ExecutedPlayer;
            summary.OriginalOrder = round.OriginalOrder;
            return summary;
        }

        public ImmutableArray<Player> VotedJa;
        public ImmutableArray<Player> VotedNein;
        public bool DidPresidentVeto;
        public bool DidChancellorVeto;
        public bool IsElectionSuccessful;
        public bool IsVoteOfPeople;
        public bool IsSpecialElection;
        public bool IsVetoed;
        public Player PresidentElect;
        public Player ChancellorElect;
        public Party ElectedPolicy;
        public ImmutableArray<Party> PresidentComment;
        public ImmutableArray<Party> ChancellorComment;
        public PresidentialPower Power;
        public Party InvestigateLoyaltyComment;
        public Player InvestigatedPlayer;
        public Player SpecialElectionChoice;
        public Player ExecutedPlayer;
        public Player OriginalOrder;
    }
}
