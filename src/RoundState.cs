using System.Collections.Immutable;

namespace Adam.SecretHitler
{
    public struct RoundState
    {
        public static readonly RoundState Initial = new RoundState
        {
            VotingPlayer = Player.None,
            Hand = ImmutableArray.Create<Party>(),
            DoesPresidentVeto = false,
            DoesChancellorVeto = false,
            PresidentDiscard = Party.None,
            ChancellorDiscard = Party.None,
            VotedJa = ImmutableArray.Create<Player>(),
            VotedNein = ImmutableArray.Create<Player>(),
            IsElectionSuccessful = false,
            IsVoteOfPeople = false,
            IsSpecialElection = false,
            IsVetoed = false,
            PresidentElect = Player.None,
            ChancellorElect = Player.None,
            ElectedPolicy = Party.None,
            PresidentComment = ImmutableArray.Create<Party>(),
            ChancellorComment = ImmutableArray.Create<Party>(),
            Power = PresidentialPower.None,
            InvestigateLoyaltyComment = Party.None,
            InvestigatedPlayer = Player.None,
            SpecialElectionChoice = Player.None,
            ExecutedPlayer = Player.None,
            ElectionTracker = -1,
            Stage = RoundStage.PassCandidacy,
            DrawPile = ImmutableArray.Create<Party>(),
            DiscardPile = ImmutableArray.Create<Party>()
        };

        public static RoundState FromGameState(GameState state)
        {
            var round = Initial;
            round.DrawPile = state.DrawPile;
            round.DiscardPile = state.DiscardPile;
            round.ElectionTracker = state.Public.ElectionTracker;
            round.Random = state.Random;
            return round;
        }

        public Player VotingPlayer;
        public ImmutableArray<Party> Hand;
        public bool DoesPresidentVeto;
        public bool DoesChancellorVeto;
        public Party PresidentDiscard;
        public Party ChancellorDiscard;
        public ImmutableArray<Player> VotedJa;
        public ImmutableArray<Player> VotedNein;
        public bool IsElectionSuccessful;
        public bool IsVoteOfPeople;
        public bool IsSpecialElection;
        public bool IsVetoed;
        public Player PresidentElect;
        public Player ChancellorElect;
        public Party ElectedPolicy;
        public ImmutableArray<Party> PresidentComment;
        public ImmutableArray<Party> ChancellorComment;
        public PresidentialPower Power;
        public Party InvestigateLoyaltyComment;
        public Player InvestigatedPlayer;
        public Player SpecialElectionChoice;
        public Player ExecutedPlayer;
        public Player OriginalOrder;
        public int ElectionTracker;
        public RoundStage Stage;
        public ImmutableRandom Random;
        public ImmutableArray<Party> DrawPile;
        public ImmutableArray<Party> DiscardPile;
    }
}
