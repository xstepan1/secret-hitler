using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Adam.SecretHitler
{
    public struct GameState
    {
        public ImmutableArray<(Role role, IStrategy strategy)> Players;
        public PublicState Public;
        public ImmutableArray<Party> DrawPile;
        public ImmutableArray<Party> DiscardPile;
        public Player Hitler;
        public ImmutableArray<Player> Fascists;
        public ImmutableRandom Random;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Piles: draw={PileToString(DrawPile, 17)},discard={PileToString(DiscardPile, 17)}");
            sb.Append("Table: ")
                .Append($"fascist={Public.FascistPolicyCount},")
                .Append($"liberal={Public.LiberalPolicyCount},")
                .Append($"tracker={Public.ElectionTracker}\n");
            var roundFlags = new (string name, bool value)[] {
                ("elected", Public.LastRound.IsElectionSuccessful),
                ("special", Public.LastRound.IsSpecialElection),
                ("people", Public.LastRound.IsVoteOfPeople),
                ("vetoed", Public.LastRound.IsVetoed)
            };
            sb.Append("Election: ");
            for (int i = 0; i < Public.PlayerCount; ++i)
            {
                if (i != 0)
                {
                    sb.Append(", ");
                }
                var playerChar = Public.Dead[i]
                    ? '-'
                    : Public.LastRound.VotedJa.Contains(new Player { Index = i })
                        ? 'J'
                        : 'N';
                sb.Append($"{PlayerToString(i)}={playerChar}");
            }
            sb.Append('\n');
            var flagString = string.Join(',', roundFlags.Where(p => p.value).Select(p => p.name));
            sb.Append("Government: ")
                .Append($"president={PlayerToString(Public.LastRound.PresidentElect.Index)},")
                .Append($"chancellor={PlayerToString(Public.LastRound.ChancellorElect.Index)},")
                .Append($"flags={flagString}\n");
            sb.Append("Legislation: ")
                .Append($"policy={PartyToString(Public.LastRound.ElectedPolicy)},")
                .Append($"president={PileToString(Public.LastRound.PresidentComment, 3)},")
                .Append($"chancellor={PileToString(Public.LastRound.ChancellorComment, 2)}\n");
            sb.Append($"Power: {Public.LastRound.Power}")
                .Append(Public.LastRound.Power switch
                {
                    PresidentialPower.InvestigateLoyalty
                        => $",investigated={PlayerToString(Public.LastRound.InvestigatedPlayer.Index)}"
                        + $",comment={Public.LastRound.InvestigateLoyaltyComment}\n",
                    PresidentialPower.CallSpecialElection
                        => $",choice={PlayerToString(Public.LastRound.SpecialElectionChoice.Index)}\n",
                    PresidentialPower.Execution
                        => $",executed={PlayerToString(Public.LastRound.ExecutedPlayer.Index)}\n",
                    _ => "\n"
                });
            return sb.ToString();
        }

        public string StrategiesToString()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < Public.PlayerCount; ++i)
            {
                sb.AppendLine($"{PlayerToString(i)}={Players[i].strategy.GetType().Name}");
            }
            return sb.ToString();
        }

        private string PlayerToString(int index)
        {
            if (index == -1)
            {
                return "[-1]";
            }

            var playerChar = RoleToString(Players[index].role);
            if (Public.Dead[index])
            {
                playerChar = playerChar.ToLower();
            }
            
            return $"[{index}{playerChar}]";
        }

        private static string PileToString(ImmutableArray<Party> pile, int minSize)
        {
            var sb = new StringBuilder("[");
            var length = pile.IsDefault ? 0 : pile.Length;
            for (int i = 0; i < length; ++i)
            {
                sb.Append(PartyToString(pile[i]));
            }
            if (minSize > length)
            {
                sb.Append(new string(' ', minSize - length));
            }

            sb.Append(']');
            return sb.ToString();
        }

        private static string RoleToString(Role role)
        {
            return role switch
            {
                Role.Hitler => "H",
                Role.Fascist => "F",
                Role.Liberal => "L" ,
                _ => "?"
            };
        }

        private static string PartyToString(Party party)
        {
            return party switch
            {
                Party.Fascist => "F",
                Party.Liberal => "L",
                _ => "?"
            };
        }
    }
}
