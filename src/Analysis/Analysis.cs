using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Adam.SecretHitler.Cfr;

namespace Adam.SecretHitler.Analysis
{
    public struct Result
    {
        string LiberalStrategy;
        string FascistStrategy;
        string HitlerStrategy;

        int FascistPolicies;
        int LiberalPolicies;
        int HitlerElected;
        int HitlerExecuted;

        int FascistWins;
        int LiberalWins;

        public Result(string libStrat, string fasStrat, string hitStat)
        {
            LiberalStrategy = libStrat;
            FascistStrategy = fasStrat;
            HitlerStrategy = hitStat;
            FascistPolicies = 0;
            LiberalPolicies = 0;
            HitlerElected = 0;
            HitlerExecuted = 0;

            FascistWins = 0;
            LiberalWins = 0;
        }

        public void Update(EndCondition end)
        {
            switch (end)
            {
                case EndCondition.FascistPolicies:
                    ++FascistPolicies;
                    ++FascistWins;
                    break;
                case EndCondition.HitlerElected:
                    ++HitlerElected;
                    ++FascistWins;
                    break;
                case EndCondition.LiberalPolicies:
                    ++LiberalPolicies;
                    ++LiberalWins;
                    break;
                case EndCondition.HitlerExecuted:
                    ++HitlerExecuted;
                    ++LiberalWins;
                    break;
            }
        }

        public static string Header()
        {
            return $"{nameof(LiberalStrategy)},{nameof(FascistStrategy)},{nameof(HitlerStrategy)},{nameof(FascistPolicies)},{nameof(LiberalPolicies)},{nameof(HitlerElected)},{nameof(HitlerExecuted)},{nameof(FascistWins)},{nameof(LiberalWins)}";
        }

        public override string ToString()
        {
            return $"{LiberalStrategy},{FascistStrategy},{HitlerStrategy},{FascistPolicies},{LiberalPolicies},{HitlerElected},{HitlerExecuted},{FascistWins},{LiberalWins}";
        }
    }

    public class GridAnalysis
    {
        private Random random;

        private Dictionary<string, Func<IStrategy>> liberalPool;
        private Dictionary<string, Func<IStrategy>> fascistPool;
        private Dictionary<string, Func<IStrategy>> hitlerPool;

        public GridAnalysis(int randomState = 42)
        {
            this.random = new Random(randomState);
            this.liberalPool = new Dictionary<string, Func<IStrategy>>{
                {"random_liberal", () => new RandomStrategy(random)},
                {"simple_liberal", () => new SimpleStrategy.Liberal()},
                {"paranoid_liberal", () => new ParanoidStrategy(random)},
                {"cfr_1Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-1000000")},
                {"cfr_9Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-9000000")},
            };
            this.fascistPool = new Dictionary<string, Func<IStrategy>>{
                {"random_fascist", () => new RandomStrategy(random)},
                {"simple_fascist", () => new SimpleStrategy.Fascist(random)},
                {"countersimple_fascist", () => new CounterSimpleStrategy(random)},
                {"paranoid_fascist", () => new ParanoidStrategy(random)},
                {"cfr_1Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-1000000")},
                {"cfr_9Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-9000000")},
            };
            this.hitlerPool = new Dictionary<string, Func<IStrategy>>{
                {"random_hitler", () => new RandomStrategy(random)},
                {"simple_hitler", () => new SimpleStrategy.Hitler()},
                {"countersimple_hitler", () => new CounterSimpleStrategy(random)},
                {"paranoid_hitler", () => new ParanoidStrategy(random)},
                {"cfr_1Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-1000000")},
                {"cfr_9Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-9000000")},
            };
        }

        public void Run(int iterations, int repeats, string outputFilename)
        {
            var results = RunGrid(iterations, repeats);
            this.ToCsv(results, outputFilename);
        }

        public void ToCsv(List<Result> results, string outputFilename)
        {
            Utils.EnsureDirectoryExists(outputFilename);

            using var file = new StreamWriter(outputFilename);
            file.WriteLine(Result.Header());
            foreach (var result in results)
            {
                file.WriteLine(result);
            }
            Console.WriteLine($"[ANAL] Output written to {outputFilename}.");
        }

        private List<Result> RunGrid(int iterations, int repeats)
        {
            ConcurrentBag<Result> results = new ConcurrentBag<Result>();
            int total = liberalPool.Count * fascistPool.Count * hitlerPool.Count;

            int current = 0;
            var combinations = from liberal in liberalPool.Keys
                               from fascist in fascistPool.Keys
                               from hitler in hitlerPool.Keys
                               select new { liberal, fascist, hitler };

            Parallel.ForEach(combinations, new ParallelOptions { MaxDegreeOfParallelism = 10 }, c => {
                Console.WriteLine($"[ANAL] strategy profile {c.hitler}, {c.fascist}, {c.liberal} ({++current}/{total})");
                var players = new List<(Role role, IStrategy strategy)>(){
                    (Role.Hitler, hitlerPool[c.hitler]()),
                    (Role.Fascist, fascistPool[c.fascist]()),
                    (Role.Fascist, fascistPool[c.fascist]()),
                    (Role.Liberal, liberalPool[c.liberal]()),
                    (Role.Liberal, liberalPool[c.liberal]()),
                    (Role.Liberal, liberalPool[c.liberal]()),
                    (Role.Liberal, liberalPool[c.liberal]()),
                    (Role.Liberal, liberalPool[c.liberal]())
                };

                for (int n = 1; n <= repeats; ++n)
                {
                    Console.WriteLine($"[ANAL] Repeat {n} out of {repeats}.");
                    var result = new Result(c.liberal, c.fascist, c.hitler);
                    for (int i = 0; i < iterations; ++i)
                    {
                        var state = Game.SetUp(ImmutableRandom.Create(), players);
                        var (winner, _, _) = Game.PlayGame(state);
                        result.Update(winner);
                    }
                    Console.WriteLine($"[ANAL] {result}");
                    results.Add(result);
                }
            });

            return results.ToList();
        }
    }
}
