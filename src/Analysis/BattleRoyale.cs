using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Adam.SecretHitler.Cfr;

namespace Adam.SecretHitler.Analysis
{
    public struct StrategyCounts
    {
        int Step;
        string Strategy;
        Role Role;
        double Count;

        public StrategyCounts(int step, string strategy, Role role, double count)
        {
            this.Step = step;
            this.Strategy = strategy;
            this.Role = role;
            this.Count = count;
        }

        public static string Header()
        {
            return $"{nameof(Step)},{nameof(Strategy)},{nameof(Role)},{nameof(Count)}";
        }

        public override string ToString()
        {
            return $"{Step},{Strategy},{Role},{Count}";
        }
    }

    public class BattleRoyale
    {
        private Random random;

        private Dictionary<string, Func<IStrategy>> liberalPool;
        private Dictionary<string, double> liberalCounts;
        private Dictionary<string, Func<IStrategy>> fascistPool;
        private Dictionary<string, double> fascistCounts;
        private Dictionary<string, Func<IStrategy>> hitlerPool;
        private Dictionary<string, double> hitlerCounts;

        private int populationSize;

        public BattleRoyale(int randomState = 42, int populationSize = 10000)
        {
            this.random = new Random(randomState);
            this.liberalPool = new Dictionary<string, Func<IStrategy>>{
                {"random_liberal", () => new RandomStrategy(random)},
                {"simple_liberal", () => new SimpleStrategy.Liberal()},
                {"paranoid_liberal", () => new ParanoidStrategy(random)},
                {"cfr_1Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-1000000")},
                {"cfr_9Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-9000000")},
            };
            this.fascistPool = new Dictionary<string, Func<IStrategy>>{
                {"random_fascist", () => new RandomStrategy(random)},
                {"simple_fascist", () => new SimpleStrategy.Fascist(random)},
                {"countersimple_fascist", () => new CounterSimpleStrategy(random)},
                {"paranoid_fascist", () => new ParanoidStrategy(random)},
                {"cfr_1Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-1000000")},
                {"cfr_9Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-9000000")},
            };
            this.hitlerPool = new Dictionary<string, Func<IStrategy>>{
                {"random_hitler", () => new RandomStrategy(random)},
                {"simple_hitler", () => new SimpleStrategy.Hitler()},
                {"countersimple_hitler", () => new CounterSimpleStrategy(random)},
                {"paranoid_hitler", () => new ParanoidStrategy(random)},
                {"cfr_1Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-1000000")},
                {"cfr_9Ms", () => new CfrTrainedStrategy(random, "trained_cfr/cfr-9000000")},
            };
            this.liberalCounts = new Dictionary<string, double>();
            this.fascistCounts = new Dictionary<string, double>();
            this.hitlerCounts = new Dictionary<string, double>();

            this.populationSize = populationSize;
            ResetPopulations();
        }

        public void Run(int steps, int repeats, string outputFilename)
        {
            var results = RunBattle(steps, repeats);
            this.ToCsv(results, outputFilename);
        }

        public void ToCsv(List<StrategyCounts> results, string outputFilename)
        {
            Utils.EnsureDirectoryExists(outputFilename);

            using var file = new StreamWriter(outputFilename);
            file.WriteLine(StrategyCounts.Header());
            foreach (var result in results)
            {
                file.WriteLine(result);
            }
            Console.WriteLine($"[ANAL-BR] Output written to {outputFilename}.");
        }

        private List<StrategyCounts> RunBattle(int steps, int repeats)
        {
            var results = new List<StrategyCounts>();

            for (int x = 1; x <= repeats; x++)
            {
                Console.WriteLine($"Repeat {x} out of {repeats}.");
                this.ResetPopulations();

                for (int i = 0; i < steps; i++)
                {
                    if (i % 1000 == 0)
                    {
                        Console.WriteLine($"\tStep {i} out of {steps}.");
                    }
                    this.AddDatapoints(i, results);
                    var strategies = SampleStrategies();
                    if (strategies.Contains("END"))
                    {
                        Console.WriteLine($"Early stopping, no alive strategies available.");
                        break;
                    }

                    var players = new List<(Role role, IStrategy strategy)>(){
                        (Role.Hitler, hitlerPool[strategies[0]]()),
                        (Role.Fascist, fascistPool[strategies[1]]()),
                        (Role.Fascist, fascistPool[strategies[2]]()),
                        (Role.Liberal, liberalPool[strategies[3]]()),
                        (Role.Liberal, liberalPool[strategies[4]]()),
                        (Role.Liberal, liberalPool[strategies[5]]()),
                        (Role.Liberal, liberalPool[strategies[6]]()),
                        (Role.Liberal, liberalPool[strategies[7]]())
                    };

                    var state = Game.SetUp(ImmutableRandom.Create(), players);
                    var (winner, _, _) = Game.PlayGame(state);
                    UpdatePopulations(winner, strategies);
                }
            }

            return results.ToList();
        }

        private void ResetPopulations()
        {
            this.liberalCounts = new Dictionary<string, double>(
                this.liberalPool.Keys.Select(p =>
                    new KeyValuePair<string, double>(p, populationSize / liberalPool.Count)));

            this.fascistCounts = new Dictionary<string, double>(
                this.fascistPool.Keys.Select(p =>
                    new KeyValuePair<string, double>(p, populationSize / fascistPool.Count)));

            this.hitlerCounts = new Dictionary<string, double>(
                this.hitlerPool.Keys.Select(p =>
                    new KeyValuePair<string, double>(p, populationSize / hitlerPool.Count)));
        }

        private void UpdatePopulations(EndCondition winner, IList<string> strategies)
        {
            if (winner == EndCondition.FascistPolicies || winner == EndCondition.HitlerElected)
            {
                this.hitlerCounts[strategies[0]] += 1;
                this.fascistCounts[strategies[1]] += 1;
                this.fascistCounts[strategies[2]] += 1;
                for (int i = 3; i < 8; i++)
                {
                    liberalCounts[strategies[i]] -= 1;
                }
            }
            else
            {
                this.hitlerCounts[strategies[0]] -= 1;
                this.fascistCounts[strategies[1]] -= 1;
                this.fascistCounts[strategies[2]] -= 1;
                for (int i = 3; i < 8; i++)
                {
                    liberalCounts[strategies[i]] += 1;
                }
            }
            this.NormalizeCounts();
        }

        private void NormalizeCounts()
        {
            var ratio = populationSize / fascistCounts.Values.Sum();
            fascistCounts = fascistCounts.ToDictionary(p => p.Key, p => p.Value * ratio);

            ratio = populationSize / hitlerCounts.Values.Sum();
            hitlerCounts = hitlerCounts.ToDictionary(p => p.Key, p => p.Value * ratio);

            ratio = populationSize / liberalCounts.Values.Sum();
            liberalCounts = liberalCounts.ToDictionary(p => p.Key, p => p.Value * ratio);
        }

        private void AddDatapoints(int step, IList<StrategyCounts> results)
        {
            foreach (var (name, count) in this.hitlerCounts)
            {
                results.Add(new StrategyCounts(step, name, Role.Hitler, count));
            }
            foreach (var (name, count) in this.fascistCounts)
            {
                results.Add(new StrategyCounts(step, name, Role.Fascist, count));
            }
            foreach (var (name, count) in this.liberalCounts)
            {
                results.Add(new StrategyCounts(step, name, Role.Liberal, count));
            }
        }

        private IList<string> SampleStrategies()
        {
            var strategies = new List<string>();
            strategies.Add(WeightedSample(this.hitlerCounts, random));
            for (int i = 0; i < 2; i++)
            {
                strategies.Add(WeightedSample(this.fascistCounts, random));
            }
            for (int i = 0; i < 5; i++)
            {
                strategies.Add(WeightedSample(this.liberalCounts, random));
            }

            return strategies;
        }

        public static string WeightedSample(IDictionary<string, double> weights, Random random)
        {
            double totalWeight = weights.Values.Sum();
            if (totalWeight <= 0)
            {
                return "END";
            }

            double pick = random.NextDouble() * totalWeight;
            foreach (var (strat, weight) in weights)
            {
                if (pick <= weight)
                {
                    return strat;
                }
                pick -= weight;
            }

            return weights.Keys.Last();
        }
    }
}
