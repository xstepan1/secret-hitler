using System;
using System.Collections.Immutable;
using System.Linq;

namespace Adam.SecretHitler
{
    public partial class SimpleStrategy
    {
        public class Hitler : IStrategy
        {
            private int[] rating = Array.Empty<int>();
            private Player self;
            private int fascistPolicyCount = 0;

            public void SetUp(PublicState state, Player self, Role role, Player hitler, ImmutableArray<Player> fascists)
            {
                if (role != Role.Hitler)
                {
                    throw new ArgumentException("This strategy only supports the Hitler role.");
                }

                this.self = self;
                rating = new int[state.PlayerCount];
                fascistPolicyCount = state.FascistPolicyCount;
            }

            public void OnRound(PublicState state)
            {
                if (state.LastRound.IsElectionSuccessful)
                {
                    int diff = state.LastRound.ElectedPolicy switch
                    {
                        Party.Liberal => -1,
                        Party.Fascist => 1,
                        _ => 0
                    };
                    rating[state.LastRound.PresidentElect.Index] += diff;
                    rating[state.LastRound.ChancellorElect.Index] += diff;
                }
                fascistPolicyCount = state.FascistPolicyCount;
            }

            public Player CallSpecialElection(ImmutableArray<Player> eligible)
            {
                return eligible.OrderByDescending(p => rating[p.Index]).First();
            }

            public Party ConcludeInvestigation(Player investigated, Party partyMembership)
            {
                if (partyMembership == Party.Liberal)
                {
                    rating[investigated.Index] -= 10;
                }
                else
                {
                    rating[investigated.Index] += 10;
                }
                return partyMembership;
            }

            public Party DiscardAsChancellor(
                ImmutableArray<Party> hand,
                out ImmutableArray<Party> chancellorComment,
                out bool wantsVeto)
            {
                if (fascistPolicyCount < 2)
                {
                    chancellorComment = hand;
                    if (hand.Contains(Party.Fascist))
                    {
                        wantsVeto = false;
                        return Party.Fascist;
                    }
                    wantsVeto = true;
                    return Party.Liberal;
                }

                if (hand.Contains(Party.Liberal))
                {
                    chancellorComment = hand.Remove(Party.Liberal).Add(Party.Fascist);
                    wantsVeto = false;
                    return Party.Liberal;
                }

                chancellorComment = hand;
                wantsVeto = true;
                return Party.Fascist;
            }

            public Party DiscardAsPresident(
                ImmutableArray<Party> hand,
                out ImmutableArray<Party> presidentComment,
                out bool wouldVeto)
            {
                if (fascistPolicyCount < 2)
                {
                    presidentComment = hand;
                    if (hand.Contains(Party.Fascist))
                    {
                        wouldVeto = false;
                        return Party.Fascist;
                    }
                    wouldVeto = true;
                    return Party.Liberal;
                }

                if (hand.Contains(Party.Liberal))
                {
                    presidentComment = hand.Remove(Party.Liberal).Add(Party.Fascist);
                    wouldVeto = false;
                    return Party.Liberal;
                }

                presidentComment = hand;
                wouldVeto = true;
                return Party.Fascist;
            }

            public Player ExecutePlayer(ImmutableArray<Player> executable)
            {
                return executable.OrderBy(p => rating[p.Index]).First();
            }

            public Player InvestigateLoyalty(ImmutableArray<Player> investigable)
            {
                return investigable.OrderByDescending(p => rating[p.Index]).First();
            }

            public Player NominateChancellor(ImmutableArray<Player> eligible)
            {
                return eligible.OrderByDescending(p => rating[p.Index]).First();
            }

            public bool Vote(Player presidentElect, Player chancellorElect)
            {
                bool isMine = presidentElect == self || chancellorElect == self;
                bool isTrustworthy = rating[presidentElect.Index] + rating[chancellorElect.Index] >= 0;
                return isMine || isTrustworthy;
            }
        }

    }
}
