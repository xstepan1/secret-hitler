﻿using System;
using System.CommandLine;
using System.Collections.Immutable;
using System.CommandLine.Invocation;
using System.Collections.Generic;
using Adam.SecretHitler.Cfr;
using Adam.SecretHitler.Analysis;
using System.Linq;

namespace Adam.SecretHitler
{
    public class Program
    {
        public const string CfrStrategyPath = "trained_cfr/cfr-1000000";
        private static readonly Random GlobalRandom = new Random();
        private static readonly ImmutableArray<Role> RoleOrder = ImmutableArray.Create(
            Role.Hitler,
            Role.Fascist,
            Role.Fascist,
            Role.Liberal,
            Role.Liberal,
            Role.Liberal,
            Role.Liberal,
            Role.Liberal);
        private static readonly ImmutableArray<(AvailableStrategy fascists, AvailableStrategy liberals)> Presets
            = ImmutableArray.Create(
                (AvailableStrategy.Random, AvailableStrategy.Random),
                (AvailableStrategy.Simple, AvailableStrategy.Random),
                (AvailableStrategy.Random, AvailableStrategy.Simple),
                (AvailableStrategy.Simple, AvailableStrategy.Simple),
                (AvailableStrategy.Paranoid, AvailableStrategy.Random),
                (AvailableStrategy.Random, AvailableStrategy.Paranoid),
                (AvailableStrategy.Paranoid, AvailableStrategy.Simple),
                (AvailableStrategy.Simple, AvailableStrategy.Paranoid),
                (AvailableStrategy.Paranoid, AvailableStrategy.Paranoid),
                (AvailableStrategy.CounterSimple, AvailableStrategy.Simple));

        public enum AvailableStrategy
        {
            None,
            Random,
            Simple,
            Cfr,
            CounterSimple,
            Paranoid
        }

        public static void RunStrategies(AvailableStrategy[] strategies, int iterations, string cfrInput)
        {
            PrintRequirements(strategies);
            var (stats, roundCount) = PlayGames(strategies, iterations, cfrInput);
            Console.WriteLine("FascistPolicies,LiberalPolicies,HitlerElected,HitlerExecuted,RoundCountSum");
            var fields = new long[]
            {
                stats[EndCondition.FascistPolicies],
                stats[EndCondition.LiberalPolicies],
                stats[EndCondition.HitlerElected],
                stats[EndCondition.HitlerExecuted],
                roundCount
            };
            Console.WriteLine(string.Join(',', fields));
        }

        public static void RunPolicies(
            AvailableStrategy[] strategies,
            int iterations,
            string cfrInput)
        {
            PrintRequirements(strategies);
            Console.WriteLine("FascistPolicyCount,LiberalPolicyCount,FascistPolicies,LiberalPolicies,"
                + "HitlerElected,HitlerExecuted,RoundCountSum");
            for (int j = 0; j <= 17; ++j)
            {
                var liberalPolicyCount = j;
                var fascistPolicyCount = 17 - j;

                var (stats, roundCount) = PlayGames(strategies, iterations, cfrInput, fascistPolicyCount, liberalPolicyCount);
                var fields = new long[]
                {
                    fascistPolicyCount,
                    liberalPolicyCount,
                    stats[EndCondition.FascistPolicies],
                    stats[EndCondition.LiberalPolicies],
                    stats[EndCondition.HitlerElected],
                    stats[EndCondition.HitlerExecuted],
                    roundCount
                };
                Console.WriteLine(string.Join(',', fields));
            }
        }

        public static void RunDebug(AvailableStrategy[] strategies, string cfrInput)
        {
            var players = CreateStrategies(strategies, cfrInput);
            var state = Game.SetUp(ImmutableRandom.Create(GlobalRandom), players);
            Console.Write(state.StrategiesToString());
            Console.WriteLine();
            var (winner, _, _) = Game.PlayGame(state, true);
            Console.WriteLine(winner);
        }

        public static void RunTrain(long itersPerSave, long itersPerUpdate, bool fullSample, string? checkpoint)
        {
            var cfr = checkpoint is null
                ? new CfrMinimizer()
                : CfrMinimizer.Load(checkpoint);

            var type = fullSample ? "full" : "random";
            var filename = $"outputs/cfr-{type}-{DateTime.Now.ToFileTimeUtc()}";
            cfr.Solve(filename, itersPerSave, itersPerUpdate, !fullSample);
        }

        public static void RunAnalysis(int iterations, int repeats, string output)
        {
            var anal = new GridAnalysis();
            anal.Run(iterations, repeats, output);
        }

        public static void RunBattleRoyale(int populationSize, int steps, int repeats, string output)
        {
            var anal = new BattleRoyale(populationSize);
            anal.Run(steps, repeats, output);
        }

        public static void RunPresets(string cfrInput, int playerCount)
        {
            const int iterations = 1000000;
            if (playerCount != 7 && playerCount != 8)
            {
                throw new ArgumentException("There can only be 7 or 8 players.");
            }
            Console.WriteLine("FascistStrategy,LiberalStrategy,FascistPolicies,LiberalPolicies,"
                + "HitlerElected,HitlerExecuted,RoundCountSum");
            foreach ((var fascistStrategy, var liberalStrategy) in Presets)
            {
                var strategies = Enumerable.Repeat(fascistStrategy, 3)
                    .Concat(Enumerable.Repeat(liberalStrategy, playerCount - 3))
                    .ToArray();
                var (stats, roundCountSum) = PlayGames(strategies, iterations, cfrInput);
                Console.WriteLine(string.Join(',', new object[] {
                    fascistStrategy,
                    liberalStrategy,
                    stats[EndCondition.FascistPolicies],
                    stats[EndCondition.LiberalPolicies],
                    stats[EndCondition.HitlerElected],
                    stats[EndCondition.HitlerExecuted],
                    roundCountSum
                }));
            }
        }

        public static void RunTracker(
            AvailableStrategy[] strategies,
            int iterations,
            string cfrInput,
            int maxTrackerLength)
        {
            PrintRequirements(strategies);
            Console.WriteLine("TrackerLength,FascistPolicies,LiberalPolicies,"
                + "HitlerElected,HitlerExecuted,RoundCountSum");
            for (int t = 1; t <= maxTrackerLength; ++t)
            {
                var (stats, roundCount) = PlayGames(strategies, iterations, cfrInput, trackerLength: t);
                var fields = new long[]
                {
                    t,
                    stats[EndCondition.FascistPolicies],
                    stats[EndCondition.LiberalPolicies],
                    stats[EndCondition.HitlerElected],
                    stats[EndCondition.HitlerExecuted],
                    roundCount
                };
                Console.WriteLine(string.Join(',', fields));
            }
        }

        public static int Main(string[] args)
        {
            var root = new RootCommand("Secret Hitler In a Terminal")
            {
                new Argument<AvailableStrategy[]>("STRATEGIES")
                {
                    Arity = new ArgumentArity(7, 8)
                },
                new Option<int>(
                    aliases: new string[] {"-i", "--iterations"},
                    getDefaultValue: () => 1000000),
                new Option<string>(
                    aliases: new string[] {"-cfr", "--cfrInput"},
                    getDefaultValue: () => CfrStrategyPath
                )
            };
            root.Handler = CommandHandler.Create<AvailableStrategy[], int, string>(RunStrategies);

            var policiesCmd = new Command("policies", "Run games with all possible policy distributions")
            {
                new Argument<AvailableStrategy[]>("STRATEGIES")
                {
                    Arity = new ArgumentArity(7, 8)
                },
                new Option<int>(
                    aliases: new string[] {"-i", "--iterations"},
                    getDefaultValue: () => 1000000),
                new Option<string>(
                    aliases: new string[] {"-cfr", "--cfrInput"},
                    getDefaultValue: () => CfrStrategyPath
                )
            };
            policiesCmd.Handler = CommandHandler.Create<AvailableStrategy[], int, string>(RunPolicies);
            root.AddCommand(policiesCmd);

            var trackerCmd = new Command("tracker", "Run games with all different length of the election tracker")
            {
                new Argument<AvailableStrategy[]>("STRATEGIES")
                {
                    Arity = new ArgumentArity(7, 8)
                },
                new Option<int>(
                    aliases: new string[] {"-i", "--iterations"},
                    getDefaultValue: () => 1000000),
                new Option<string>(
                    aliases: new string[] {"-cfr", "--cfrInput"},
                    getDefaultValue: () => CfrStrategyPath),
                new Option<int>(
                    aliases: new string[] {"-m", "--max-tracker-length"},
                    getDefaultValue: () => 12)
            };
            trackerCmd.Handler = CommandHandler.Create<AvailableStrategy[], int, string, int>(RunTracker);
            root.AddCommand(trackerCmd);

            var debugCmd = new Command("debug", "Run a single, very verbose game")
            {
                new Argument<AvailableStrategy[]>("STRATEGIES")
                {
                    Arity = new ArgumentArity(7, 8)
                },
                new Option<string>(
                    aliases: new string[] {"--cfrInput"},
                    getDefaultValue: () => CfrStrategyPath
                )
            };
            debugCmd.Handler = CommandHandler.Create<AvailableStrategy[], string>(RunDebug);
            root.AddCommand(debugCmd);

            var trainCmd = new Command("train", "Train a strategy using the CFR algorithm") {
                new Option<long>(new string[] {"-ips", "--itersPerSave"}, () => 1000000),
                new Option<long>(new string[] {"-ipu", "--itersPerUpdate"}, () => 100000),
                new Option<bool>(new string[] {"-f", "--fullSample"}, () => false),
                new Option<string?>(new string[] {"-c", "--checkpoint"}, () => null)
            };
            trainCmd.Handler = CommandHandler.Create<long, long, bool, string?>(RunTrain);
            root.AddCommand(trainCmd);

            var analyzeCmd = new Command("analysis", "Run all strategies against each other") {
                new Option<int>(new string[] {"-i", "--iterations"}, () => 100000),
                new Option<int>(new string[] {"-n", "--repeats"}, () => 10),
                new Option<string>(new string[] {"-o", "--output"}, () => "results/results.csv"),
            };
            analyzeCmd.Handler = CommandHandler.Create<int, int, string>(RunAnalysis);
            root.AddCommand(analyzeCmd);

            var battleRoyaleCmd = new Command("battleroyale", "Make strategies play to the death") {
                new Option<int>(new string[] {"-p", "--populationSize"}, () => 10000),
                new Option<int>(new string[] {"-s", "--steps"}, () => 100000),
                new Option<int>(new string[] {"-n", "--repeats"}, () => 5),
                new Option<string>(new string[] {"-o", "--output"}, () => "results/results_br.csv"),
            };
            battleRoyaleCmd.Handler = CommandHandler.Create<int, int, int, string>(RunBattleRoyale);
            root.AddCommand(battleRoyaleCmd);

            var presetsCmd = new Command("presets", "Analyze hard-coded setups")
            {
                new Option<int>(
                    alias: "--playerCount",
                    getDefaultValue: () => 8),
                new Option<string>(
                    aliases: new string[] {"--cfrInput"},
                    getDefaultValue: () => CfrStrategyPath
                )
            };
            presetsCmd.Handler = CommandHandler.Create<string, int>(RunPresets);
            root.AddCommand(presetsCmd);

            return root.Invoke(args);
        }

        private static ImmutableArray<(Role role, IStrategy strategy)> CreateStrategies(
            AvailableStrategy[] requirements,
            string cfrInput)
        {
            if (requirements.Length != 7 && requirements.Length != 8)
            {
                throw new ArgumentException("Only the version for 7 or 8 players is supported.");
            }

            var builder = ImmutableArray.CreateBuilder<(Role role, IStrategy strategy)>();
            for (int i = 0; i < requirements.Length; ++i)
            {
                IStrategy strategy = requirements[i] switch
                {
                    AvailableStrategy.Random => new RandomStrategy(GlobalRandom),
                    AvailableStrategy.Simple => new SimpleStrategy(GlobalRandom),
                    AvailableStrategy.Cfr => new CfrTrainedStrategy(GlobalRandom, cfrInput),
                    AvailableStrategy.CounterSimple => new CounterSimpleStrategy(GlobalRandom),
                    AvailableStrategy.Paranoid => new ParanoidStrategy(GlobalRandom),
                    _ => new NotImplementedStrategy()
                };

                builder.Add((RoleOrder[i], strategy));
            }
            return builder.ToImmutable();
        }

        private static (ImmutableDictionary<EndCondition, int>, long roundCountSum) PlayGames(
            AvailableStrategy[] strategies,
            int iterations,
            string cfrInput,
            int fascistPolicyCount = 11,
            int liberalPolicyCount = 6,
            int trackerLength = 3)
        {
            var stats = new Dictionary<EndCondition, int>
            {
                [EndCondition.FascistPolicies] = 0,
                [EndCondition.LiberalPolicies] = 0,
                [EndCondition.HitlerElected] = 0,
                [EndCondition.HitlerExecuted] = 0
            };
            long roundCountSum = 0;
            for (int i = 0; i < iterations; ++i)
            {
                var players = CreateStrategies(strategies, cfrInput);
                var state = Game.SetUp(
                    ImmutableRandom.Create(GlobalRandom),
                    players,
                    fascistPolicyCount,
                    liberalPolicyCount,
                    trackerLength);
                var (winner, _, roundCount) = Game.PlayGame(state);
                roundCountSum += roundCount;
                stats[winner]++;
            }
            return (stats.ToImmutableDictionary(), roundCountSum);
        }

        private static void PrintRequirements(AvailableStrategy[] requirements)
        {
            for (int i = 0; i < requirements.Length; ++i)
            {
                Console.WriteLine($"{RoleOrder[i]}: {requirements[i]}");
            }
        }
    }
}
