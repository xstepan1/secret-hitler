using System;
using System.Collections.Immutable;
using System.Linq;

namespace Adam.SecretHitler
{
    public partial class SimpleStrategy : StrategyWrapper
    {
        private readonly Random random;

        public SimpleStrategy(Random random)
        {
            this.random = random;
        }

        public override void SetUp(
            PublicState state,
            Player self,
            Role role,
            Player hitler,
            ImmutableArray<Player> fascists)
        {
            inner = role switch
            {
                Role.Liberal => new Liberal(),
                Role.Fascist => new Fascist(random),
                Role.Hitler => new Hitler(),
                _ => throw new ArgumentException($"Role '{role}' is not a supported."),
            };
            base.SetUp(state, self, role, hitler, fascists);
        }

    }
}
