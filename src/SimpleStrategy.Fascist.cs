using System;
using System.Collections.Immutable;
using System.Linq;

namespace Adam.SecretHitler
{
    public partial class SimpleStrategy
    {
        public class Fascist : IStrategy
        {
            private Player self = Player.None;
            private Player hitler;
            private ImmutableArray<Player> allies = default;
            private readonly Random random;
            private int fascistPolicyCount = 0;

            public Fascist(Random random)
            {
                this.random = random;
            }

            public void SetUp(PublicState state, Player self, Role role, Player hitler, ImmutableArray<Player> fascists)
            {
                if (role != Role.Fascist)
                {
                    throw new ArgumentException("This strategy only supports the Fascist role.");
                }
                this.self = self;
                this.hitler = hitler;
                allies = fascists.Add(hitler);
                fascistPolicyCount = state.FascistPolicyCount;
            }

            public void OnRound(PublicState state)
            {
                fascistPolicyCount = state.FascistPolicyCount;
            }

            public Player CallSpecialElection(ImmutableArray<Player> eligible)
            {
                var buddies = eligible.Intersect(allies).ToImmutableArray();
                if (buddies.Length > 0)
                {
                    return buddies[random.Next(0, buddies.Length)];
                }

                return eligible[random.Next(0, eligible.Length)];
            }

            public Party ConcludeInvestigation(Player investigated, Party partyMembership)
            {
                return Party.Fascist;
            }

            public Party DiscardAsChancellor(
                ImmutableArray<Party> hand,
                out ImmutableArray<Party> chancellorComment,
                out bool wantsVeto)
            {
                if (hand.Contains(Party.Liberal))
                {
                    chancellorComment = hand.Remove(Party.Liberal).Add(Party.Fascist);
                    wantsVeto = false;
                    return Party.Liberal;
                }

                chancellorComment = hand;
                wantsVeto = true;
                return Party.Fascist;
            }

            public Party DiscardAsPresident(
                ImmutableArray<Party> hand,
                out ImmutableArray<Party> presidentComment,
                out bool wouldVeto)
            {
                if (hand.Contains(Party.Liberal))
                {
                    presidentComment = hand.Remove(Party.Liberal).Add(Party.Fascist);
                    wouldVeto = false;
                    return Party.Liberal;
                }

                presidentComment = hand;
                wouldVeto = true;
                return Party.Fascist;
            }

            public Player ExecutePlayer(ImmutableArray<Player> executable)
            {
                var candidates = executable.Except(allies).ToImmutableArray();
                if (candidates.Length > 0)
                {
                    return candidates[random.Next(0, candidates.Length)];
                }

                return executable[random.Next(0, executable.Length)];
            }

            public Player InvestigateLoyalty(ImmutableArray<Player> investigable)
            {
                var candidates = investigable.Except(allies).ToImmutableArray();
                if (candidates.Length > 0)
                {
                    return candidates[random.Next(0, candidates.Length)];
                }

                return investigable[random.Next(0, investigable.Length)];
            }

            public Player NominateChancellor(ImmutableArray<Player> eligible)
            {
                if (fascistPolicyCount >= 3 && eligible.Contains(hitler))
                {
                    return hitler;
                }

                var ideal = eligible.Intersect(allies).ToImmutableArray();
                if (ideal.Length > 0)
                {
                    return ideal[random.Next(0, ideal.Length)];
                }

                return eligible[random.Next(0, eligible.Length)];
            }

            public bool Vote(Player presidentElect, Player chancellorElect)
            {
                bool isMine = presidentElect == self || chancellorElect == self;
                bool isAllied = allies.Contains(presidentElect) || allies.Contains(chancellorElect);
                return isMine || isAllied;
            }
        }
    }
}
