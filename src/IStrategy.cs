using System.Collections.Generic;
using System.Collections.Immutable;

namespace Adam.SecretHitler
{
    public interface IStrategy
    {
        void SetUp(PublicState state, Player self, Role role, Player hitler, ImmutableArray<Player> fascists);
        void OnRound(PublicState state);
        Player NominateChancellor(ImmutableArray<Player> eligible);
        Party DiscardAsPresident(
            ImmutableArray<Party> hand,
            out ImmutableArray<Party> presidentComment,
            out bool wouldVeto);
        Party DiscardAsChancellor(
            ImmutableArray<Party> hand,
            out ImmutableArray<Party> chancellorComment,
            out bool wantsVeto);
        bool Vote(Player presidentElect, Player chancellorElect);
        Player InvestigateLoyalty(ImmutableArray<Player> investigable);
        Party ConcludeInvestigation(Player investigated, Party partyMembership);
        Player CallSpecialElection(ImmutableArray<Player> eligible);
        Player ExecutePlayer(ImmutableArray<Player> executable);
    }
}
