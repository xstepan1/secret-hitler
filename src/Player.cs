using System;
using System.Diagnostics.CodeAnalysis;

namespace Adam.SecretHitler
{
    public struct Player : IEquatable<Player>
    {
        public static readonly Player None = new Player { Index = -1 };

        public int Index;


        public bool Equals([AllowNull] Player other)
        {
            return Index == other.Index;
        }

        public override bool Equals(object? obj)
        {
            if (obj is Player player)
            {
                return Equals(player);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Index);
        }

        public override string? ToString()
        {
            return $"Player[{Index}]";
        }

        public static implicit operator int(Player player)
        {
            return player.Index;
        }

        public static bool operator ==(Player left, Player right)
        {
            return left.Index == right.Index;
        }

        public static bool operator !=(Player left, Player right)
        {
            return left.Index != right.Index;
        }
    }
}
