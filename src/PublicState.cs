using System;
using System.Collections.Immutable;
using System.Text;

namespace Adam.SecretHitler
{
    public struct PublicState
    {
        public int PlayerCount;
        public int FascistPolicyCount;
        public int LiberalPolicyCount;
        public Player LastElectedPresident;
        public Player LastElectedChancellor;
        public ImmutableArray<bool> Dead;
        public int ElectionTracker;
        public int TrackerLength;
        public RoundSummary LastRound;
    }
}
