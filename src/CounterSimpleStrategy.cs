using System;
using System.Collections.Immutable;
using System.Linq;

namespace Adam.SecretHitler
{
    public class CounterSimpleStrategy : StrategyWrapper
    {
        private readonly Random random;

        public CounterSimpleStrategy(Random random)
        {
            this.random = random;
        }

        public override void SetUp(
            PublicState state,
            Player self,
            Role role,
            Player hitler,
            ImmutableArray<Player> fascists)
        {
            inner = role switch
            {
                Role.Fascist => new Fascist(random),
                Role.Hitler => new Hitler(),
                _ => throw new ArgumentException($"Role '{role}' is not supported."),
            };
            base.SetUp(state, self, role, hitler, fascists);
        }

        public class Fascist : IStrategy
        {
            private Player self = Player.None;
            private Player hitler;
            private ImmutableArray<Player> allies = default;
            private readonly Random random;
            private PublicState state;

            public Fascist(Random random)
            {
                this.random = random;
            }

            public void SetUp(PublicState state, Player self, Role role, Player hitler, ImmutableArray<Player> fascists)
            {
                if (role != Role.Fascist)
                {
                    throw new ArgumentException("This strategy only supports the Fascist role.");
                }
                this.self = self;
                this.hitler = hitler;
                allies = fascists.Add(hitler);
                this.state = state;
            }

            public void OnRound(PublicState state)
            {
                this.state = state;
            }

            public Player CallSpecialElection(ImmutableArray<Player> eligible)
            {
                var buddies = eligible.Intersect(allies).ToImmutableArray();
                if (buddies.Length > 0)
                {
                    return buddies[random.Next(0, buddies.Length)];
                }

                return eligible[random.Next(0, eligible.Length)];
            }

            public Party ConcludeInvestigation(Player investigated, Party partyMembership)
            {
                return Party.Fascist;
            }

            public Party DiscardAsChancellor(
                ImmutableArray<Party> hand,
                out ImmutableArray<Party> chancellorComment,
                out bool wantsVeto)
            {
                int liberals = hand.Count(p => p == Party.Liberal);
                wantsVeto = false;
                chancellorComment = hand;

                if (liberals == 0) {
                    return Party.Fascist;
                }
                if (liberals == 2) {
                    wantsVeto = state.FascistPolicyCount == 5;
                    return Party.Liberal;
                }

                if (state.LiberalPolicyCount >= 3) {
                    chancellorComment = hand.Remove(Party.Liberal).Add(Party.Fascist);
                    return Party.Liberal;
                }

                return Party.Fascist;
            }

            public Party DiscardAsPresident(
                ImmutableArray<Party> hand,
                out ImmutableArray<Party> presidentComment,
                out bool wouldVeto)
            {
                int liberals = hand.Count(p => p == Party.Liberal);
                presidentComment = hand;
                wouldVeto = false;

                if (liberals == 3) {
                    return Party.Liberal;
                }
                if (liberals == 0) {
                    return Party.Fascist;
                }

                if (state.LiberalPolicyCount >= 3) {
                    presidentComment = hand.Remove(Party.Liberal).Add(Party.Fascist);
                    return Party.Liberal;
                }
                
                return Party.Fascist;
            }

            public Player ExecutePlayer(ImmutableArray<Player> executable)
            {
                var candidates = executable.Except(allies).ToImmutableArray();
                if (candidates.Length > 0)
                {
                    return candidates[random.Next(0, candidates.Length)];
                }

                return executable[random.Next(0, executable.Length)];
            }

            public Player InvestigateLoyalty(ImmutableArray<Player> investigable)
            {
                var candidates = investigable.Except(allies).ToImmutableArray();
                if (candidates.Length > 0)
                {
                    return candidates[random.Next(0, candidates.Length)];
                }

                return investigable[random.Next(0, investigable.Length)];
            }

            public Player NominateChancellor(ImmutableArray<Player> eligible)
            {
                if (state.FascistPolicyCount >= 3 && eligible.Contains(hitler))
                {
                    return hitler;
                }

                var ideal = eligible.Intersect(allies).ToImmutableArray();
                if (ideal.Contains(hitler)) {
                    ideal.Remove(hitler);
                }
                if (ideal.Length > 0)
                {
                    return ideal[random.Next(0, ideal.Length)];
                }

                return eligible[random.Next(0, eligible.Length)];
            }

            public bool Vote(Player presidentElect, Player chancellorElect)
            {
                int distance = self.Index - presidentElect.Index;
                if (distance < 0) {
                    distance += state.PlayerCount;
                }
                // maximising presidency
                if (chancellorElect != self && distance <= 2) {
                    return false;
                }

                bool isMine = presidentElect == self || chancellorElect == self;
                bool isAllied = allies.Contains(presidentElect) || allies.Contains(chancellorElect);
                return isMine || isAllied;
            }
        }

        public class Hitler : IStrategy
        {
            private Player self = Player.None;
            private int[] rating = Array.Empty<int>();
            private PublicState state;

            public void SetUp(PublicState state, Player self, Role role, Player hitler, ImmutableArray<Player> fascists)
            {
                if (role != Role.Hitler)
                {
                    throw new ArgumentException("This strategy only supports the Liberal role.");
                }
                this.self = self;
                this.state = state;
                rating = new int[state.PlayerCount];
            }

            public void OnRound(PublicState state)
            {
                if (state.LastRound.IsElectionSuccessful)
                {
                    int diff = state.LastRound.ElectedPolicy switch
                    {
                        Party.Liberal => 1,
                        Party.Fascist => -1,
                        _ => 0
                    };
                    rating[state.LastRound.PresidentElect.Index] += diff;
                    rating[state.LastRound.ChancellorElect.Index] += diff;
                }
                this.state = state;
            }

            public Player CallSpecialElection(ImmutableArray<Player> eligible)
            {
                return eligible.OrderByDescending(p => rating[p.Index]).First();
            }

            public Party ConcludeInvestigation(Player investigated, Party partyMembership)
            {
                if (partyMembership == Party.Liberal)
                {
                    rating[investigated.Index] += 10;
                }
                else
                {
                    rating[investigated.Index] -= 10;
                }
                return partyMembership;
            }

            public Party DiscardAsChancellor(
                ImmutableArray<Party> hand,
                out ImmutableArray<Party> chancellorComment,
                out bool wantsVeto)
            {
                int liberals = hand.Count(p => p == Party.Liberal);
                wantsVeto = false;
                chancellorComment = hand;

                if (state.LiberalPolicyCount >= 3) {
                    if (liberals == 2) {
                        wantsVeto = true;
                    }
                    if (liberals > 0) {
                        chancellorComment = hand.Remove(Party.Liberal).Add(Party.Fascist);
                        return Party.Liberal;
                    }
                }

                if (hand.Contains(Party.Fascist))
                {
                    wantsVeto = false;
                    return Party.Fascist;
                }
                wantsVeto = true;
                return Party.Liberal;
            }

            public Party DiscardAsPresident(
                ImmutableArray<Party> hand,
                out ImmutableArray<Party> presidentComment,
                out bool wouldVeto)
            {
                wouldVeto = false;
                presidentComment = hand;

                int liberals = hand.Count(p => p == Party.Liberal);
                if (liberals == 0) {
                    return Party.Fascist;
                }
                if (liberals == 3) {
                    return Party.Liberal;
                }

                if (state.FascistPolicyCount >= 5 && state.LastElectedChancellor.Index >= 0) {
                    if (liberals == 1 || rating[state.LastElectedChancellor.Index] < 0) {
                        return Party.Liberal;
                    }
                }

                if (state.LiberalPolicyCount >= 4) {
                    if (liberals == 3) {
                        wouldVeto = true;
                    }
                    presidentComment = hand.Remove(Party.Liberal).Add(Party.Fascist);
                    return Party.Liberal;
                }

                if (hand.Contains(Party.Fascist))
                {
                    wouldVeto = false;
                    return Party.Fascist;
                }
                wouldVeto = true;
                return Party.Liberal;
            }

            public Player ExecutePlayer(ImmutableArray<Player> executable)
            {
                return executable.OrderBy(p => rating[p.Index]).Last();
            }

            public Player InvestigateLoyalty(ImmutableArray<Player> investigable)
            {
                return investigable.OrderBy(p => rating[p.Index]).First();
            }

            public Player NominateChancellor(ImmutableArray<Player> eligible)
            {
                if (state.FascistPolicyCount >= 5) {
                    return eligible.OrderBy(p => rating[p.Index]).First();
                }
                return eligible.OrderByDescending(p => rating[p.Index]).First();
            }

            public bool Vote(Player presidentElect, Player chancellorElect)
            {
                bool isMine = presidentElect == self || chancellorElect == self;
                bool isTrustworthy = rating[presidentElect.Index] + rating[chancellorElect.Index] >= 0;
                return isMine || isTrustworthy;
            }
        }
    }
}
