using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Adam.SecretHitler
{
    public static class Game
    {
        public static GameState SetUp(
            ImmutableRandom random,
            IEnumerable<(Role role, IStrategy strategy)> players,
            int fascistPolicyCount = 11,
            int liberalPolicyCount = 6,
            int trackerLength = 3)
        {
            if (players.Count() != 7 && players.Count() != 8
                || players.Count(p => p.role == Role.Fascist) != 2
                || players.Count(p => p.role == Role.Hitler) != 1)
            {
                throw new ArgumentException(
                    "The players provided cannot play a game for 7-8 players.",
                    nameof(players));
            }

            var publicState = new PublicState
            {
                PlayerCount = players.Count(),
                FascistPolicyCount = 0,
                LiberalPolicyCount = 0,
                ElectionTracker = 0,
                LastElectedPresident = Player.None,
                LastElectedChancellor = Player.None,
                Dead = Enumerable.Repeat(false, players.Count()).ToImmutableArray(),
                LastRound = RoundSummary.Initial,
                TrackerLength = trackerLength
            };

            var statePlayers = players.ToArray();
            Shuffle(ref random, statePlayers);
            var hitler = new Player { Index = Array.FindIndex(statePlayers, p => p.role == Role.Hitler) };
            var fascists = statePlayers.Select((p, i) => (p.role, i))
                .Where(p => p.role == Role.Fascist)
                .Select(p => new Player { Index = p.i })
                .ToImmutableArray();

            for (int i = 0; i < statePlayers.Length; ++i)
            {
                statePlayers[i].strategy.SetUp(
                    state: publicState,
                    self: new Player { Index = i },
                    role: statePlayers[i].role,
                    hitler: statePlayers[i].role == Role.Fascist ? hitler : Player.None,
                    fascists: statePlayers[i].role == Role.Fascist ? fascists : default);
            }

            var drawPile = new List<Party>(17);
            drawPile.AddRange(Enumerable.Repeat(Party.Liberal, liberalPolicyCount));
            drawPile.AddRange(Enumerable.Repeat(Party.Fascist, fascistPolicyCount));
            Shuffle(ref random, drawPile);

            return new GameState
            {
                Public = publicState,
                DiscardPile = ImmutableArray.Create<Party>(),
                DrawPile = drawPile.ToImmutableArray(),
                Fascists = fascists,
                Hitler = hitler,
                Players = statePlayers.ToImmutableArray(),
                Random = random
            };
        }

        public static void Shuffle<T>(ref ImmutableRandom random, IList<T> pile)
        {
            for (int i = 0; i != pile.Count; ++i)
            {
                int shuffleIndex = random.Next(pile.Count);
                var tmp = pile[i];
                pile[i] = pile[shuffleIndex];
                pile[shuffleIndex] = tmp;
            }
        }

        public static (EndCondition end, GameState finalState, int roundCount) PlayGame(
            GameState state,
            bool verbose = false)
        {
            if (verbose)
            {
                Console.WriteLine(state);
            }

            var winner = GetWinner(state);
            int roundCount = 0;
            while (winner == EndCondition.None)
            {
                state = PlayRound(state);
                roundCount++;
                for (int i = 0; i < state.Public.PlayerCount; ++i)
                {
                    if (!state.Public.Dead[i])
                    {
                        state.Players[i].strategy.OnRound(state.Public);
                    }
                }
                if (verbose)
                {
                    Console.WriteLine(state);
                }

                winner = GetWinner(state);
            }
            return (winner, state, roundCount);
        }

        public static EndCondition GetWinner(GameState state)
        {
            if (state.Public.FascistPolicyCount == 6)
            {
                return EndCondition.FascistPolicies;
            }

            if (state.Public.LiberalPolicyCount == 5)
            {
                return EndCondition.LiberalPolicies;
            }

            if (state.Public.Dead[state.Hitler.Index])
            {
                return EndCondition.HitlerExecuted;
            }

            if (state.Public.FascistPolicyCount >= 3
                && state.Public.LastRound.IsElectionSuccessful
                && state.Public.LastRound.ChancellorElect == state.Hitler)
            {
                return EndCondition.HitlerElected;
            }

            return EndCondition.None;
        }

        /// <summary>
        /// ELECTION
        /// 1. Pass the presidential candidacy
        /// </summary>
        private static void PassCandidacy(GameState state, ref RoundState round)
        {
            round.PresidentElect.Index = (state.Public.LastRound.PresidentElect.Index + 1) % state.Public.PlayerCount;
            if (state.Public.LastRound.Power == PresidentialPower.CallSpecialElection)
            {
                round.IsSpecialElection = true;
                round.OriginalOrder = state.Public.LastRound.PresidentElect;
                round.PresidentElect = state.Public.LastRound.SpecialElectionChoice;
            }
            else if (state.Public.LastRound.IsSpecialElection)
            {
                round.PresidentElect.Index = (state.Public.LastRound.OriginalOrder.Index + 1)
                    % state.Public.PlayerCount;
            }

            round.Stage = RoundStage.NominateChancellor;
        }

        /// <summary>
        /// ELECTION
        /// 2. Nominate a chancellor
        /// </summary>
        private static void NominateChancellor(GameState state, ref RoundState round)
        {
            var chancellorEligible = GetChancellorEligible(ref state, round.PresidentElect);
            round.ChancellorElect = state.Players[round.PresidentElect.Index].strategy
                .NominateChancellor(chancellorEligible);
            if (!chancellorEligible.Contains(round.ChancellorElect))
            {
                throw new InvalidOperationException(
                    $"Strategy {round.PresidentElect.Index} picked an uneligible player for its chancellor.");
            }

            round.VotingPlayer = new Player { Index = 0};
            round.Stage = RoundStage.Vote;
        }

        private static void VotePeople(ref RoundState round)
        {
            round.IsVoteOfPeople = true;
            round.ElectedPolicy = Draw(ref round, 1).Single();
            round.ElectionTracker = 0;
            round.Stage = RoundStage.End;
        }

        /// <summary>
        /// ELECTION
        /// 3. Vote on the government
        /// </summary>
        private static void VoteOnGovernment(GameState state, ref RoundState round)
        {
            if (state.Players[round.VotingPlayer.Index].strategy.Vote(round.PresidentElect, round.ChancellorElect))
            {
                round.VotedJa = round.VotedJa.Add(round.VotingPlayer);
            }
            else
            {
                round.VotedNein = round.VotedNein.Add(round.VotingPlayer);
            }

            round.VotingPlayer.Index++;
            if (round.VotingPlayer.Index < state.Public.PlayerCount)
            {
                // election is not over yet
                return;
            }

            round.IsElectionSuccessful = round.VotedJa.Length > round.VotedNein.Length;
            if (!round.IsElectionSuccessful)
            {
                round.ElectionTracker++;
                round.Stage = RoundStage.End;
                if (round.ElectionTracker >= state.Public.TrackerLength)
                {
                    // vote failed and people are MAD
                    VotePeople(ref round);
                }
                return;
            }

            round.Stage = RoundStage.DiscardAsPresident;
            round.ElectionTracker = 0;
        }

        /// <summary>
        /// LEGISLATIVE SESSION
        /// The President discards one out of three policy cards.
        /// </summary>
        private static void DiscardAsPresident(GameState state, ref RoundState round)
        {
            round.Hand = Draw(ref round, 3);
            round.PresidentDiscard = state.Players[round.PresidentElect.Index].strategy
                .DiscardAsPresident(
                    hand: round.Hand,
                    presidentComment: out round.PresidentComment,
                    wouldVeto: out round.DoesPresidentVeto);
            if (!round.Hand.Contains(round.PresidentDiscard))
            {
                throw new InvalidOperationException("President discarded a non-existent policy.");
            }

            round.Hand = round.Hand.Remove(round.PresidentDiscard);
            round.DiscardPile = round.DiscardPile.Add(round.PresidentDiscard);
            round.Stage = RoundStage.DiscardAsChancellor;
        }

        /// <summary>
        /// LEGISLATIVE SESSION
        /// The Chancellor discards one of the two remaining policy cards.
        /// </summary>
        private static void DiscardAsChancellor(GameState state, ref RoundState round)
        {
            round.ChancellorDiscard = state.Players[round.ChancellorElect.Index].strategy.DiscardAsChancellor(
                hand: round.Hand,
                chancellorComment: out round.ChancellorComment,
                wantsVeto: out round.DoesChancellorVeto);
            if (!round.Hand.Contains(round.ChancellorDiscard))
            {
                throw new InvalidOperationException("Chancellor discarded a non-existent policy.");
            }

            round.Hand = round.Hand.Remove(round.ChancellorDiscard);
            round.DiscardPile = round.DiscardPile.Add(round.ChancellorDiscard);

            if (state.Public.FascistPolicyCount == 5
                && round.DoesChancellorVeto
                && round.DoesPresidentVeto)
            {
                // vetoed
                round.IsVetoed = true;
                round.ElectionTracker++;

                if (round.ElectionTracker >= state.Public.TrackerLength)
                {
                    VotePeople(ref round);
                    return;
                }
            }

            round.ElectedPolicy = round.Hand.Single();
            round.Hand.Clear();
            round.Stage = RoundStage.End;
            if (round.ElectedPolicy != Party.Fascist)
            {
                return;
            }

            var newFascistCount = state.Public.FascistPolicyCount + 1;
            switch (newFascistCount)
            {
                case 2:
                    round.Power = PresidentialPower.InvestigateLoyalty;
                    round.Stage = RoundStage.ExecutiveAction;
                    return;
                case 3:
                    round.Power = PresidentialPower.CallSpecialElection;
                    round.Stage = RoundStage.ExecutiveAction;
                    return;
                case 4:
                case 5:
                    round.Power = PresidentialPower.Execution;
                    round.Stage = RoundStage.ExecutiveAction;
                    return;
            }
        }

        /// <summary>
        /// EXECUTIVE ACTION
        /// The President takes an Executive Action.
        /// </summary>
        private static void TakeExecutiveAction(GameState state, ref RoundState round)
        {
            switch (round.Power)
            {
                case PresidentialPower.InvestigateLoyalty:
                    var investigable = GetInvestigableSpecialElectable(ref state, round.PresidentElect);
                    round.InvestigatedPlayer = state.Players[round.PresidentElect.Index].strategy
                        .InvestigateLoyalty(investigable);
                    if (!investigable.Contains(round.InvestigatedPlayer))
                    {
                        throw new InvalidOperationException("President picked an invalid player for investigation.");
                    }

                    round.Stage = RoundStage.ConcludeInvestigation;
                    return;
                case PresidentialPower.CallSpecialElection:
                    var specialElectable = GetInvestigableSpecialElectable(ref state, round.PresidentElect);
                    round.SpecialElectionChoice = state.Players[round.PresidentElect.Index].strategy
                        .CallSpecialElection(specialElectable);
                    if (!specialElectable.Contains(round.SpecialElectionChoice))
                    {
                        throw new InvalidOperationException("President picked an invalid player for special election.");
                    }

                    round.Stage = RoundStage.End;
                    return;
                case PresidentialPower.Execution:
                    var executable = GetExecutablePlayers(state);
                    round.ExecutedPlayer = state.Players[round.PresidentElect.Index].strategy
                        .ExecutePlayer(executable);
                    if (!executable.Contains(round.ExecutedPlayer))
                    {
                        throw new InvalidOperationException("President tried to execute an unexecutable player.");
                    }

                    state.Public.Dead = state.Public.Dead.SetItem(round.ExecutedPlayer.Index, true);
                    round.Stage = RoundStage.End;
                    return;
                default:
                    throw new InvalidOperationException("There's no executive power to execute.");
            }
        }

        /// <summary>
        /// EXECUTIVE ACTION
        /// Conclude loyalty investigation.
        /// TODO: Is this really necessary?
        /// </summary>
        private static void ConcludeInvestigation(GameState state, ref RoundState round)
        {
            round.InvestigateLoyaltyComment = state.Players[round.PresidentElect.Index].strategy
                .ConcludeInvestigation(
                    round.InvestigatedPlayer,
                    GetPartyMembership(ref state, round.InvestigatedPlayer));

            round.Stage = RoundStage.End;
        }

        public static void NextMove(GameState state, ref RoundState round)
        {
            switch (round.Stage)
            {
                case RoundStage.PassCandidacy:
                    PassCandidacy(state, ref round);
                    break;
                case RoundStage.NominateChancellor:
                    NominateChancellor(state, ref round);
                    break;
                case RoundStage.Vote:
                    VoteOnGovernment(state, ref round);
                    break;
                case RoundStage.DiscardAsPresident:
                    DiscardAsPresident(state, ref round);
                    break;
                case RoundStage.DiscardAsChancellor:
                    DiscardAsChancellor(state, ref round);
                    break;
                case RoundStage.ExecutiveAction:
                    TakeExecutiveAction(state, ref round);
                    break;
                case RoundStage.ConcludeInvestigation:
                    ConcludeInvestigation(state, ref round);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state));
            }
        }

        public static GameState ApplyRound(GameState state, RoundState round)
        {
            state.DrawPile = round.DrawPile;
            state.DiscardPile = round.DiscardPile;
            state.Random = round.Random;
            state.Public.ElectionTracker = round.ElectionTracker;
            if (round.IsElectionSuccessful)
            {
                state.Public.LastElectedPresident = round.PresidentElect;
                state.Public.LastElectedChancellor = round.ChancellorElect;
            }

            if (round.IsVoteOfPeople)
            {
                state.Public.LastElectedPresident = Player.None;
                state.Public.LastElectedChancellor = Player.None;
            }

            if (round.IsElectionSuccessful || round.IsVoteOfPeople)
            {
                switch (round.ElectedPolicy)
                {
                    case Party.Fascist:
                        state.Public.FascistPolicyCount++;
                        break;
                    case Party.Liberal:
                        state.Public.LiberalPolicyCount++;
                        break;
                    default:
                        throw new InvalidOperationException($"WTF is the '{Party.None}' policy?");
                }
            }

            if (round.Power == PresidentialPower.Execution)
            {
                state.Public.Dead = state.Public.Dead.SetItem(round.ExecutedPlayer.Index, true);
            }
            state.Public.LastRound = RoundSummary.FromRoundState(round);
            return state;
        }

        public static GameState PlayRound(GameState state)
        {
            var round = RoundState.FromGameState(state);
            while (round.Stage != RoundStage.End)
            {
                NextMove(state, ref round);
            }
            return ApplyRound(state, round);
        }

        public static ImmutableArray<Player> GetChancellorEligible(ref GameState state, Player president)
        {
            var eligible = ImmutableArray.CreateBuilder<Player>();
            for (int i = 0; i < state.Public.PlayerCount; ++i)
            {
                if (i != president.Index
                    && !state.Public.Dead[i]
                    && i != state.Public.LastElectedPresident.Index
                    && i != state.Public.LastElectedChancellor.Index)
                {
                    eligible.Add(new Player { Index = i });
                }
            }
            return eligible.ToImmutable();
        }

        public static ImmutableArray<Player> GetInvestigableSpecialElectable(ref GameState state, Player president)
        {
            var eligible = ImmutableArray.CreateBuilder<Player>();
            for (int i = 0; i < state.Public.PlayerCount; ++i)
            {
                if (i != president.Index && !state.Public.Dead[i])
                {
                    eligible.Add(new Player { Index = i });
                }
            }
            return eligible.ToImmutable();
        }

        public static ImmutableArray<Player> GetExecutablePlayers(GameState state)
        {
            return Enumerable.Range(0, state.Public.PlayerCount - 1)
                .Where(i => !state.Public.Dead[i])
                .Select(i => new Player { Index = i })
                .ToImmutableArray();
        }

        public static Party GetPartyMembership(ref GameState state, Player player)
        {
            return state.Players[player.Index].role switch
            {
                Role.Fascist => Party.Fascist,
                Role.Hitler => Party.Fascist,
                Role.Liberal => Party.Liberal,
                _ => Party.None,
            };
        }

        public static ImmutableArray<Party> Draw(ref RoundState round, int count)
        {
            if (count > round.DrawPile.Length + round.DiscardPile.Length)
            {
                throw new ArgumentException("There are not enough cards in the piles.");
            }

            var builder = ImmutableArray.CreateBuilder<Party>();
            while (builder.Count < count)
            {
                if (round.DrawPile.Length < 3)
                {
                    var tmp = ImmutableArray.CreateBuilder<Party>();
                    tmp.AddRange(round.DiscardPile);
                    tmp.AddRange(round.DrawPile);
                    Shuffle(ref round.Random, tmp);
                    round.DrawPile = tmp.ToImmutable();
                    round.DiscardPile = ImmutableArray.Create<Party>();
                }
                var takeCount = Math.Min(round.DrawPile.Length, count);
                builder.AddRange(round.DrawPile.TakeLast(takeCount));
                round.DrawPile = round.DrawPile.Take(round.DrawPile.Length - takeCount)
                    .ToImmutableArray();
            }
            return builder.ToImmutable();
        }
    }
}
