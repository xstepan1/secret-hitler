namespace Adam.SecretHitler
{
    public enum PresidentialPower
    {
        None,
        InvestigateLoyalty,
        CallSpecialElection,
        Execution
    }
}
