using System.Linq;
using System.Collections.Immutable;

namespace Adam.SecretHitler
{
    public static class SH
    {
        public static readonly ImmutableArray<Party> FascistTwo
            = ImmutableArray.Create(Party.Fascist, Party.Fascist);

        public static readonly ImmutableArray<Party> FascistThree
            = ImmutableArray.Create(Party.Fascist, Party.Fascist, Party.Fascist);

        public static bool DoesStoryCheckOut(
            Party electedPolicy,
            ImmutableArray<Party> presidentComment,
            ImmutableArray<Party> chancellorComment)
        {
            if (!presidentComment.IsDefault && presidentComment.Length == 3
                && !chancellorComment.IsDefault && chancellorComment.Length == 2)
            {
                presidentComment = presidentComment.Remove(electedPolicy);
                presidentComment = presidentComment.Remove(chancellorComment[0]);
                presidentComment = presidentComment.Remove(chancellorComment[1]);
                return presidentComment.Length == 0;
            }

            return false;
        }
    }
}
