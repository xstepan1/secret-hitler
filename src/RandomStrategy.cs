using System;
using System.Collections.Immutable;

namespace Adam.SecretHitler
{
    public class RandomStrategy : IStrategy
    {
        private readonly Random random;

        public RandomStrategy(Random random)
        {
            this.random = random;
        }

        public Player CallSpecialElection(ImmutableArray<Player> eligiblePlayers)
        {
            return eligiblePlayers[random.Next(0, eligiblePlayers.Length)];
        }

        public Party ConcludeInvestigation(Player investigated, Party partyMembership)
        {
            return Party.None;
        }

        public Party DiscardAsChancellor(ImmutableArray<Party> hand, out ImmutableArray<Party> chancellorComment, out bool wantsVeto)
        {
            chancellorComment = default;
            wantsVeto = random.Next(0, 100) < 50;
            return hand[random.Next(0, hand.Length)];
        }

        public Party DiscardAsPresident(ImmutableArray<Party> hand, out ImmutableArray<Party> presidentComment, out bool wouldVeto)
        {
            presidentComment = default;
            wouldVeto = random.Next(0, 100) < 50;
            return hand[random.Next(0, hand.Length)];
        }

        public Player ExecutePlayer(ImmutableArray<Player> executable)
        {
            return executable[random.Next(0, executable.Length)];
        }

        public Player InvestigateLoyalty(ImmutableArray<Player> investigable)
        {
            return investigable[random.Next(0, investigable.Length)];
        }

        public void OnRound(PublicState state)
        {
        }

        public Player NominateChancellor(ImmutableArray<Player> eligible)
        {
            return eligible[random.Next(0, eligible.Length)];
        }

        public void SetUp(PublicState state, Player self, Role role, Player hitler, ImmutableArray<Player> fascists)
        {
        }

        public bool Vote(Player presidentElect, Player chancellorElect)
        {
            return random.Next(0, 100) < 50;
        }
    }
}
