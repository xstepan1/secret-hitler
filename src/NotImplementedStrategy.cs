using System;
using System.Collections.Immutable;

namespace Adam.SecretHitler
{
    public class NotImplementedStrategy : IStrategy
    {
        public Player CallSpecialElection(ImmutableArray<Player> eligible)
        {
            throw new NotImplementedException();
        }

        public Party ConcludeInvestigation(Player investigated, Party partyMembership)
        {
            throw new NotImplementedException();
        }

        public Party DiscardAsChancellor(ImmutableArray<Party> hand, out ImmutableArray<Party> chancellorComment, out bool wantsVeto)
        {
            throw new NotImplementedException();
        }

        public Party DiscardAsPresident(ImmutableArray<Party> hand, out ImmutableArray<Party> presidentComment, out bool wouldVeto)
        {
            throw new NotImplementedException();
        }

        public Player ExecutePlayer(ImmutableArray<Player> executable)
        {
            throw new NotImplementedException();
        }

        public Player InvestigateLoyalty(ImmutableArray<Player> investigable)
        {
            throw new NotImplementedException();
        }

        public void OnRound(PublicState state)
        {
            throw new NotImplementedException();
        }

        public Player NominateChancellor(ImmutableArray<Player> eligible)
        {
            throw new NotImplementedException();
        }

        public void SetUp(PublicState state, Player self, Role role, Player hitler, ImmutableArray<Player> fascists)
        {
            throw new NotImplementedException();
        }

        public bool Vote(Player presidentElect, Player chancellorElect)
        {
            throw new NotImplementedException();
        }
    }
}
